from multiprocessing.dummy import dict
from typing import Collection, Union
from pylode import __version__
from pylode.common import TEMPLATES_DIR, STYLE_DIR
import collections
from os import path
from itertools import chain
import markdown
from jinja2 import Environment, FileSystemLoader
from os.path import join
from rdflib import URIRef, BNode, Literal
from rdflib import collection as rdflibCollection
from rdflib.namespace import DC, DCTERMS, DOAP, OWL, PROF, PROV, RDF, RDFS, SDO, SKOS
from rdflib.term import URIRef as NodeRef
from pylode.profiles.base import BaseProfile
import re

class OntDoc(BaseProfile):
    def __init__(
            self,
            g,
            source_info,
            outputformat="html",
            include_css=False,
            default_language="en",
            use_curies_stored=False,
            get_curies_online=False,
            link=None,
            sphn_connector=False
    ):
        super().__init__(
            g,
            source_info,
            outputformat=outputformat,
            include_css=include_css,
            use_curies_stored=use_curies_stored,
            get_curies_online=get_curies_online,
            link=link,
            default_language=default_language)
        self.G.bind("prov", PROV)
        self.CLASSES = collections.OrderedDict()
        self.PROPERTIES = collections.OrderedDict()
        self.NAMED_INDIVIDUALS = collections.OrderedDict()
        self.LOOPS = collections.OrderedDict()
        self.sphn_connector = sphn_connector

    def _make_collection_class_html(self, col_type, col_members):
        if col_type == "owl:unionOf":
            j = " or "
        elif col_type == "owl:intersectionOf":
            j = " and "
        else:
            j = " ? "
        # others...
        return "({})".format(
            j.join([self._make_formatted_uri(x, type="c") for x in col_members])
        )

    def _make_restriction_html(self, subject, restriction_bn):
        prop = None
        card = None
        cls = None

        for p2, o2 in self.G.predicate_objects(subject=restriction_bn):
            if p2 != RDF.type:
                if p2 == OWL.onProperty:
                    # TODO: add the property type for HTML
                    t = None
                    if str(o2) in self.PROPERTIES.keys():
                        t = self.PROPERTIES[str(o2)]["prop_type"]
                    prop = self._make_formatted_uri(str(o2), t)
                elif p2 == OWL.onClass:
                    """
                    domains = []
                    for o in self.G.objects(subject=s, predicate=RDFS.domain):
                        if type(o) != BNode:
                            domains.append(str(o))  # domains that are just classes
                        else:
                            # domain collections (unionOf | intersectionOf
                            q = '''
                                PREFIX owl:  <http://www.w3.org/2002/07/owl#>
                                PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>

                                SELECT ?col_type ?col_member
                                WHERE {{
                                    <{}> rdfs:domain ?domain .
                                    ?domain owl:unionOf|owl:intersectionOf ?collection .
                                    ?domain ?col_type ?collection .
                                    ?collection rdf:rest*/rdf:first ?col_member .
                                }}
                            '''.format(s)
                            collection_type = None
                            collection_members = []
                            for r in self.G.query(q):
                                collection_type = self._get_curie(str(r.col_type))
                                collection_members.append(str(r.col_member))
                            domains.append((collection_type, collection_members))
                    self.PROPERTIES[prop]['domains'] = domains

                    """
                    if type(o2) == BNode:
                        # onClass collections (unionOf | intersectionOf
                        q = """
                            PREFIX owl:  <http://www.w3.org/2002/07/owl#>
                            PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>

                            SELECT ?col_type ?col_member
                            WHERE {{
                                <{}> owl:onClass ?onClass .
                                ?onClass owl:unionOf|owl:intersectionOf ?collection .
                                ?onClass ?col_type ?collection .
                                ?collection rdf:rest*/rdf:first ?col_member .
                            }}
                        """.format(
                            str(subject)
                        )
                        collection_type = None
                        collection_members = []
                        for r in self.G.query(q):
                            collection_type = self._get_curie(str(r.col_type))
                            collection_members.append(str(r.col_member))

                        cls = self._make_collection_class_html(
                            collection_type, collection_members
                        )
                    else:
                        cls = self._make_formatted_uri(str(o2), type="c")
                elif p2 in [
                    OWL.cardinality,
                    OWL.qualifiedCardinality,
                    OWL.minCardinality,
                    OWL.minQualifiedCardinality,
                    OWL.maxCardinality,
                    OWL.maxQualifiedCardinality,
                ]:
                    if p2 in [OWL.minCardinality, OWL.minQualifiedCardinality]:
                        card = "min"
                    elif p2 in [OWL.maxCardinality, OWL.maxQualifiedCardinality]:
                        card = "max"
                    elif p2 in [OWL.cardinality, OWL.qualifiedCardinality]:
                        card = "exactly"

                    if self.outputformat in ["md", "adoc"]:
                        card = '**{}** {}'.format(
                            card, str(o2)
                        )
                    else:
                        card = '<span class="cardinality">{}</span> {}'.format(
                            card, str(o2)
                        )
                elif p2 in [OWL.allValuesFrom, OWL.someValuesFrom]:
                    if p2 == OWL.allValuesFrom:
                        card = "only"
                    else:  # p2 == OWL.someValuesFrom
                        card = "some"

                    if type(o2) == BNode:
                        # someValuesFrom collections (unionOf | intersectionOf
                        q = """
                            PREFIX owl:  <http://www.w3.org/2002/07/owl#>
                            PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>

                            SELECT ?col_type ?col_member
                            WHERE {{
                                <{0}> ?x _:{1} .
                                _:{1} owl:someValuesFrom|owl:allValuesFrom ?bn2 .
                                ?bn2 owl:unionOf|owl:intersectionOf ?collection .
                                ?s ?col_type ?collection .
                                ?collection rdf:rest*/rdf:first ?col_member .
                            }}
                        """.format(
                            str(subject), str(o2)
                        )
                        collection_type = None
                        collection_members = []
                        for r in self.G.query(q):
                            collection_type = self._get_curie(str(r.col_type))
                            collection_members.append(str(r.col_member))

                        c = self._make_collection_class_html(
                            collection_type, collection_members
                        )
                    else:
                        c = self._make_formatted_uri(str(o2), type="c")

                    if self.outputformat in ["md", "adoc"]:
                        card = '**{}** {}'.format(card, c)
                    else:
                        card = '<span class="cardinality">{}</span> {}'.format(card, c)
                elif p2 == OWL.hasValue:
                    if self.outputformat in ["md", "adoc"]:
                        card = '**value** {}'.format(
                            self._make_formatted_uri(str(o2), type="c")
                        )
                    else:
                        card = '<span class="cardinality">value</span> {}'.format(
                            self._make_formatted_uri(str(o2), type="c")
                        )

        restriction = prop + " " + card if card is not None else prop
        restriction = restriction + " " + cls if cls is not None else restriction

        return restriction

    def _load_template(self, template_file):
        return Environment(loader=FileSystemLoader(join(TEMPLATES_DIR, "ontdoc"))).get_template(template_file)

    def _make_fragment_uri(self, uri):
        """OntDoc Profile allows fragment URIs for Classes & Properties"""
        if self.PROPERTIES.get(uri) or self.CLASSES.get(uri):
            if self.PROPERTIES.get(uri):
                title = self.PROPERTIES[uri]["title"] \
                    if self.PROPERTIES[uri].get("title") is not None else self.PROPERTIES[uri]["fid"]
                uri = self.PROPERTIES[uri]["fid"]
            elif self.CLASSES.get(uri):
                title = self.CLASSES[uri]["title"] \
                    if self.CLASSES[uri].get("title") is not None else self.CLASSES[uri]["fid"]
                uri = self.CLASSES[uri]["fid"]

            links = {
                "md": f"[{title}](#{uri})",
                "adoc": f"link:#{uri}[{title}]",
                "html": f'<a href="#{uri}">{title}</a>',
                "rq": f'<a href="#{uri}">{title}</a>'
            }

            return links[self.outputformat]
        else:
            return self._make_formatted_uri_basic(uri)

    def _make_sphn_source_file_reference(self):
        # !!!!!!!!!! Update to new repo
        # if you are on the master, then return the master, otherwise return the right version
        return '<a href="https://git.dcc.sib.swiss/sphn-semantic-framework/sphn-schema/-/raw/master/rdf_schema/sphn_rdf_schema.ttl?inline=false">RDF (turtle)</a>'

    def _make_ps_source_file_reference(self):
        # return the link to the published rdf schema of the project-specific schema
        link = None
        if self.link is not None: link = '<a href="' + str(self.link) + '">RDF (turtle)</a>'
        return link

    def _make_external_formatted_uri(self, uri):
        """
        This function might be a bit gone. A lot of trial and error went into this it seems
        """
        base_uri=str(self.METADATA.get("uri"))
        base_uri_sep = "#"
        if base_uri.endswith("#"):
            base_uri_sep = "#"
        elif base_uri.endswith("/"):
            base_uri_sep = "/"
        else:
            base_uri = base_uri + "#"
        html_header='<a href="'
        html_sep='">'
        html_trailer='</a>'
        name = self.G.value(URIRef(uri), RDFS.label)
        # if we have a Named Individual, we would also like to generate local links -- TODO: have a look not sure that works
        if (uri in self.NAMED_INDIVIDUALS.keys()):
            return html_header + "#" +  uri.split(base_uri_sep)[1] + html_sep + self.NAMED_INDIVIDUALS[uri]["title"] + html_trailer
        # if there is a simple link inside the document, we use the provided linking
        elif uri.startswith(base_uri) and len(uri) > len(base_uri):
            return super()._make_formatted_uri(uri)
        # it there is a link outside of the document and we have a name for it, we apply a simple own linking, except for sphn because
        # those are most likely classes that were extended so we link locally
        elif (name != None):
            if 'sphn#' in uri:
                return html_header + "https://www.biomedit.ch/rdf/sphn-schema/sphn#" +  uri.split(base_uri_sep)[1] + html_sep + name + html_trailer
            # Check for unresolvable links and eliminate them.
            return self._make_link_if_resolvable(uri, name) #html_header + uri + html_sep + name + html_trailer
        else:
            # in case everything fails, we use the safe fallback
            # Build the external link check into it with an additional adapter for naming conventions
            name = super()._make_formatted_uri(uri)
            name = name[:-1].split('>')[-1].split('<')[0]
            return self._make_link_if_resolvable(uri, name)

    def _make_formatted_uri(self, uri, type=None):
        link = self._make_external_formatted_uri(uri)

        types = {
            "c": "class",
            "op": "object property",
            "fp": "functional property",
            "dp": "datatype property",
            "ap": "annotation property",
            "ni": "named individual"
        }

        if type not in types.keys():
            return link

        suffixes = {
            "md": f' ({type})',
            "adoc": f' ^{type}^',
            "html": f'<sup class="sup-{type}" title="{types[type]}">{type}</sup>',
            "rq": f'<sup class="sup-{type}" title="{types[type]}">{type}</sup>'
        }

        return link + suffixes[self.outputformat]

    def _expand_graph(self):
        # name
        for s, o in chain(
                self.G.subject_objects(DC.title),
                self.G.subject_objects(RDFS.label),
                self.G.subject_objects(SKOS.prefLabel),
                self.G.subject_objects(SDO.name)
        ):
            self.G.add((s, DCTERMS.title, o))

        # description
        # compatibility for sphn older than 2022.1
        for s in self.G.subjects(predicate=RDF.type, object=OWL.Ontology):
            for p, o in self.G.predicate_objects(subject=s):
                if p == OWL.versionIRI:
                    self.METADATA["versionIRI"] = self._make_formatted_uri(o)
        version_check = self.METADATA.get("versionIRI")

        try: year = int(version_check.split("/")[6])
        except: year = int(2023)

        if 2023 > year:
            for s, o in chain(
                    self.G.subject_objects(DC.description),
                    self.G.subject_objects(RDFS.comment),
                    self.G.subject_objects(SDO.description)
            ):
                self.G.add((s, DCTERMS.description, o))
        else:
            for s, o in chain(
                    self.G.subject_objects(DC.description),
                    self.G.subject_objects(SKOS.definition),
                    self.G.subject_objects(SDO.description)
            ):
                self.G.add((s, DCTERMS.description, o))            

        # property types
        for s in chain(
                self.G.subjects(RDF.type, OWL.ObjectProperty),
                self.G.subjects(RDF.type, OWL.FunctionalProperty),
                self.G.subjects(RDF.type, OWL.DatatypeProperty),
                self.G.subjects(RDF.type, OWL.AnnotationProperty)
        ):
            self.G.add((s, RDF.type, RDF.Property))

        # class types
        for s in self.G.subjects(RDF.type, OWL.Class):
            self.G.add((s, RDF.type, RDFS.Class))

        # owl:Restrictions from Blank Nodes
        for s in self.G.subjects(OWL.onProperty):
            self.G.add((s, RDF.type, OWL.Restriction))

        # Agents
        # creator
        for s, o in chain(
                self.G.subject_objects(DC.creator),
                self.G.subject_objects(SDO.creator),
                self.G.subject_objects(SDO.author)  # conflate SDO.author with DCTERMS.creator
        ):
            self.G.remove((s, DC.creator, o))
            self.G.remove((s, SDO.creator, o))
            self.G.remove((s, SDO.author, o))
            self.G.add((s, DCTERMS.creator, o))

        # contributor
        for s, o in chain(
                self.G.subject_objects(DC.contributor),
                self.G.subject_objects(SDO.contributor)
        ):
            self.G.remove((s, DC.contributor, o))
            self.G.remove((s, SDO.contributor, o))
            self.G.add((s, DCTERMS.contributor, o))

        # publisher
        for s, o in chain(
                self.G.subject_objects(DC.publisher),
                self.G.subject_objects(SDO.publisher)
        ):
            self.G.remove((s, DC.publisher, o))
            self.G.remove((s, SDO.publisher, o))
            self.G.add((s, DCTERMS.publisher, o))

    def _extract_metadata(self):

        self.METADATA["has_classes"] = False

        if len(self.CLASSES.keys()) > 0:
            self.METADATA["has_classes"] = True

        self.METADATA["has_ops"] = False
        self.METADATA["has_fps"] = False
        self.METADATA["has_dps"] = False
        self.METADATA["has_aps"] = False
        self.METADATA["has_ps"] = False

        if len(self.NAMED_INDIVIDUALS.keys()) > 0:
            self.METADATA["has_nis"] = True

        for k, v in self.PROPERTIES.items():
            if v.get("prop_type") == "op":
                self.METADATA["has_ops"] = True
            if v.get("prop_type") == "fp":
                self.METADATA["has_fps"] = True
            if v.get("prop_type") == "dp":
                self.METADATA["has_dps"] = True
            if v.get("prop_type") == "ap":
                self.METADATA["has_aps"] = True
            if v.get("prop_type") == "p":
                self.METADATA["has_ps"] = True

        s_str = None
        self.METADATA["imports"] = set()
        self.METADATA["creators"] = set()
        self.METADATA["contributors"] = set()
        self.METADATA["publishers"] = set()
        self.METADATA["editors"] = set()
        self.METADATA["funders"] = set()
        self.METADATA["translators"] = set()
        for s in self.G.subjects(predicate=RDF.type, object=OWL.Ontology):
            s_str = str(s)  # this is the Schema's URI
            self.METADATA["uri"] = s_str
            self.METADATA["sphn-uri"] = s_str

            for p, o in self.G.predicate_objects(subject=s):
                if p == OWL.imports:
                    self.METADATA["imports"].add(self._make_link_if_resolvable(str(o)))
                    if str(o).startswith("https://biomedit.ch/rdf/sphn-ontology/sphn/"):
                        self.METADATA["sphn-uri"] = "https://biomedit.ch/rdf/sphn-ontology/sphn"
                    elif str(o).startswith("https://biomedit.ch/rdf/sphn-schema/sphn/"):
                        self.METADATA["sphn-uri"] = "https://biomedit.ch/rdf/sphn-schema/sphn"

                if p == DCTERMS.title:
                    self.METADATA["title"] = str(o)

                if p == DCTERMS.description:
                    if self.outputformat == "md":
                        self.METADATA["description"] = str(o)
                    elif self.outputformat == "adoc":
                        self.METADATA["description"] = str(o)
                    else:
                        self.METADATA["description"] = markdown.markdown(str(o))

                if p == SKOS.historyNote:
                    if self.outputformat == "md":
                        self.METADATA["historyNote"] = str(o)
                    elif self.outputformat == "adoc":
                        self.METADATA["historyNote"] = str(o)
                    else:
                        self.METADATA["historyNote"] = markdown.markdown(str(o))

                # dates
                if p in [DCTERMS.created, DCTERMS.modified, DCTERMS.issued]:
                    date_type = p.split("/")[-1]
                    self.METADATA[date_type] = str(o)

                if p == DCTERMS.source:
                    if str(o).startswith('http'):
                        self.METADATA["source"] = self._make_formatted_uri(o)
                    else:
                        self.METADATA["source"] = str(o)

                if p == OWL.versionIRI:
                    self.METADATA["versionIRI"] = self._make_formatted_uri(o)

                if p == OWL.priorVersion:
                    self.METADATA["priorVersion"] = self._make_formatted_uri(o)

                if p == OWL.versionInfo:
                    self.METADATA["versionInfo"] = str(o)

                if p == URIRef("http://purl.org/vocab/vann/preferredNamespacePrefix"):
                    self.METADATA["preferredNamespacePrefix"] = str(o)

                if p == URIRef("http://purl.org/vocab/vann/preferredNamespaceUri"):
                    self.METADATA["preferredNamespaceUri"] = str(o)

                if p == DCTERMS.license:
                    self.METADATA["license"] = (
                        self._make_formatted_uri(o)
                        if str(o).startswith("http")
                        else str(o)
                    )

                if p == DCTERMS.rights:
                    self.METADATA["rights"] = (
                        str(o)
                            .replace("Copyright", "&copy;")
                            .replace("copyright", "&copy;")
                            .replace("(c)", "&copy;")
                    )

                # Agents
                if p in [
                    DCTERMS.creator, DCTERMS.contributor, DCTERMS.publisher, SDO.editor, SDO.funder, SDO.translator
                ]:
                    agent_type = p.split("/")[-1] + "s"
                    if type(o) == Literal:
                        self.METADATA[agent_type].add(str(o))
                    else:  # Blank Node or URI
                        self.METADATA[agent_type].add(self._make_agent(o))

                if p == PROV.wasGeneratedBy:
                    for o2 in self.G.objects(subject=o, predicate=DOAP.repository):
                        self.METADATA["repository"] = self._make_formatted_uri(o2)

                if p == SDO.codeRepository:
                    self.METADATA["repository"] = self._make_formatted_uri(o)

            if self.METADATA.get("title") is None:
                self.METADATA["title"] = "{no title found}"

        if self.METADATA["sphn-uri"] not in ["https://biomedit.ch/rdf/sphn-ontology/sphn", "https://biomedit.ch/rdf/sphn-schema/sphn"]:
            raise Exception(
                "Your RDF file either is not an SPHN schema or it does not import the expected SPHN schema. "
            )

        if s_str is None:
            raise Exception(
                "Your RDF file does not define a RDF schema. "
                "It must contains a declaration such as <...> rdf:type owl:Ontology ."
            )

    def _extract_classes_uris(self):
        classes = []
        for s in self.G.subjects(predicate=RDF.type, object=RDFS.Class):
            # ignore blank nodes for things like [ owl:unionOf ( ... ) ]
            if type(s) == BNode:
                pass
            else:
                classes.append(str(s))

        for p in sorted(classes):
            self.CLASSES[p] = {}
    
    def _make_link_if_resolvable(self, o, name=None):
        unresolvable = 'https://biomedit.ch/rdf/sphn-resource'
        unresolvable_icdo_3 = 'https://data.jrc.ec.europa.eu/collection/ICDO3_'
        if name:
            if str(o).startswith(unresolvable) or str(o).startswith(unresolvable_icdo_3):
                return '<span>' + name + '</span>'
            return '<a href="'+str(o)+'">'+ name +'</a>'

        if str(o).startswith(unresolvable) or str(o).startswith(unresolvable_icdo_3):
            return '<span>' + str(o) + '</span>'
        return '<a href="'+str(o)+'">'+str(o)+'</a>'

    def _add_links_for_recommended_values(self, o):
        """
        In case the scopeNote looks something like: ABC recommended values: DEF
        extract snomed codes from DEF part and add in a link that refers to the snomed website. 
        Currently only supports snomed codes.
        
        """

        Query = """
        SELECT ?s ?o
        WHERE {
        ?s rdfs:label ?o .
        }
        """
        results = self.G.query(Query)

        link_prefix = 'http://snomed.info/id/'
        if 'recommended values:' not in o:
            return o

        href, elements = o.split('recommended values:')
        element_list = elements.split(',')

        label_code_list = []
        for part in element_list:
            code = part.split('|')[0].strip()
            if ' ' in code:
                code = code.split(' ')[-1]
            label = part.split('|')[1].strip()
            label_code_list.append((code, label))

        for code, label in label_code_list:
            for row in results:
                if str(row.s).endswith(code) and label in str(row.o):
                    if str(row.o).startswith("SNOMED"):
                        pattern = re.compile(rf'(SNOMED CT:\s)?{re.escape(code)}\s*\|\s*{re.escape(label)}\s*\|')
                        replacement = f'<a href="{link_prefix + code}">SNOMED {code} | {label} |</a></a><sup class="sup-c" title="class">c</sup>'
                        o = re.sub(pattern, replacement, o)
        return o

    def _extract_sensitive_fields(self):
        # there should be only one thing which is called subjectToDeidentification
        subjectToDeidentificationProperty = ""
        deidUris = set()
        for subject, predicate, object in self.G.triples((None, None, None)):
                if str(predicate).endswith("subjectToDeIdentification"):
                    subjectToDeidentificationProperty = predicate
                    # if it is a property add it to the property, if it is at a class add iot to the class
                    typeObject = list(self.G.objects(subject, RDF.type))[0] if len (list(self.G.objects(subject, RDF.type))) > 0 else ""
                    if typeObject == OWL.Class :
                        self.CLASSES[str(subject)]["SUBJECTTODEIDENTIFICATION"] = True
                        deidUris.add(str(subject))
                    elif typeObject == OWL.DatatypeProperty or typeObject == OWL.ObjectProperty or typeObject == RDF.Property :
                        self.PROPERTIES[str(subject)]["SUBJECTTODEIDENTIFICATION"] = True
                        deidUris.add(str(subject))
                    else:
                        print("Somehow the type is neither a property not a class for the uri " + str(subject))
        return 0


    def _extract_classes(self):
        for cls in self.CLASSES.keys():
            s = URIRef(cls)
            # create Python dict for each class
            self.CLASSES[cls] = {
                "iri": cls
            }

            # basic class properties
            self.CLASSES[cls]["title"] = None
            self.CLASSES[cls]["description"] = None
            self.CLASSES[cls]["definition"] = []
            self.CLASSES[cls]["scopeNote"] = []
            self.CLASSES[cls]["examples"] = []
            self.CLASSES[cls]["isDefinedBy"] = []
            self.CLASSES[cls]["source"] = None

            for p, o in self.G.predicate_objects(subject=s):
                if p == DCTERMS.title:
                    self.CLASSES[cls]["title"] = str(o)

                if p == DCTERMS.description:
                    if self.outputformat == "md":
                        self.CLASSES[cls]["description"] = str(o)
                    elif self.outputformat == "adoc":
                        self.CLASSES[cls]["description"] = str(o)
                    else:
                        self.CLASSES[cls]["description"] = markdown.markdown(str(o))

                # compatibility for sphn older than 2022.1
                for onto in self.G.subjects(predicate=RDF.type, object=OWL.Ontology):
                    for pr, ob in self.G.predicate_objects(subject=onto):
                        if pr == OWL.versionIRI:
                            self.METADATA["versionIRI"] = self._make_formatted_uri(ob)
                version_check = self.METADATA.get("versionIRI")

                try: year = int(version_check.split("/")[6])
                except: year = int(2023)

                if 2023 > year:
                    if p == SKOS.definition:
                        if self.outputformat == "md":
                            self.CLASSES[cls]["definition"].append(str(o))
                        elif self.outputformat == "adoc":
                            self.CLASSES[cls]["definition"].append(str(o))
                        else:
                            self.CLASSES[cls]["definition"].append(markdown.markdown(str(o)))

                if p == SKOS.scopeNote:
                    if self.outputformat == "md":
                        self.CLASSES[cls]["definition"].append(str(o))
                    elif self.outputformat == "adoc":
                        self.CLASSES[cls]["definition"].append(str(o))
                    else:
                        self.CLASSES[cls]["definition"].append(markdown.markdown(str(o)))
                
                if p == SKOS.note:
                    o = self._add_links_for_recommended_values(o)
                    if self.outputformat == "md":
                        self.CLASSES[cls]["scopeNote"].append(str(o))
                    elif self.outputformat == "adoc":
                        self.CLASSES[cls]["scopeNote"].append(str(o))
                    else:
                        self.CLASSES[cls]["scopeNote"].append(markdown.markdown(str(o)))

                if p == SKOS.example:
                    self.CLASSES[cls]["examples"].append(self._make_example(o))

                if p == RDFS.isDefinedBy:
                    self.CLASSES[cls]["isDefinedBy"].append(str(o))

                if p == DCTERMS.source or p == DC.source:
                    if str(o).startswith('http'):
                        self.CLASSES[cls]["source"] = self._make_formatted_uri(o)
                    else:
                        self.CLASSES[cls]["source"] = str(o)

            # patch title from URI if we haven't got one
            if self.CLASSES[cls]["title"] is None:
                self.CLASSES[cls]["title"] = self._make_title_from_uri(cls)

            # make fid
            if "#" in cls:
                self.CLASSES[cls]["fid"] = cls.split("#")[1]
            else:
                self.CLASSES[cls]["fid"] = self._make_fid(self.CLASSES[cls]["title"], cls)

            # equivalent classes
            equivalent_classes = []
            for o in self.G.objects(subject=s, predicate=OWL.equivalentClass):
                if type(o) != BNode:
                    equivalent_classes.append(
                        str(o)
                    )  # ranges that are just classes
                else:
                    # equivalent classes collections (unionOf | intersectionOf
                    q = """
                        PREFIX owl:  <http://www.w3.org/2002/07/owl#>
                        PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>

                        SELECT ?col_type ?col_member
                        WHERE {{
                            <{}> owl:equivalentClass ?eq .
                            ?eq owl:unionOf|owl:intersectionOf ?collection .
                            ?eq ?col_type ?collection .
                            ?collection rdf:rest*/rdf:first ?col_member .
                        }}
                    """.format(
                        s
                    )
                    collection_type = None
                    collection_members = []
                    for r in self.G.query(q):
                        collection_type = self._get_curie(str(r.col_type))
                        collection_members.append(self._get_curie(str(r.col_member)))
                    equivalent_classes.append((collection_type, collection_members))
            for o in self.G.subjects(object=s, predicate=OWL.equivalentClass):
                if type(o) != BNode:
                    equivalent_classes.append(
                        str(o)
                    )  # ranges that are just classes
                else:
                    # equivalent classes collections (unionOf | intersectionOf
                    q = """
                        PREFIX owl:  <http://www.w3.org/2002/07/owl#>
                        PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>

                        SELECT ?col_type ?col_member
                        WHERE {{
                            <{}> owl:equivalentClass ?eq .
                            ?eq owl:unionOf|owl:intersectionOf ?collection .
                            ?eq ?col_type ?collection .
                            ?collection rdf:rest*/rdf:first ?col_member .
                        }}
                    """.format(
                        s
                    )
                    collection_type = None
                    collection_members = []
                    for r in self.G.query(q):
                        collection_type = self._get_curie(str(r.col_type))
                        collection_members.append(self._get_curie(str(r.col_member)))
                    equivalent_classes.append((collection_type, collection_members))
            self.CLASSES[cls]["equivalents"] = equivalent_classes

            # super classes
            supers = []
            restrictions = []
            for o in self.G.objects(subject=s, predicate=RDFS.subClassOf):
                if (o, RDF.type, OWL.Restriction) not in self.G:
                    if type(o) != BNode:
                        supers.append(str(o))  # supers that are just classes
                    else:
                        # super collections (unionOf | intersectionOf
                        q = """
                            PREFIX owl:  <http://www.w3.org/2002/07/owl#>
                            PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>

                            SELECT ?col_type ?col_member
                            WHERE {{
                                <{}> rdfs:subClassOf ?sup .
                                ?sup owl:unionOf|owl:intersectionOf ?collection .
                                ?sup ?col_type ?collection .
                                ?collection rdf:rest*/rdf:first ?col_member .
                            }}
                        """.format(
                            s
                        )
                        collection_type = None
                        collection_members = []
                        for r in self.G.query(q):
                            collection_type = self._get_curie(str(r.col_type))
                            collection_members.append(str(r.col_member))
                else:
                    restrictions.append(o)

            self.CLASSES[cls]["supers"] = supers
            self.CLASSES[cls]["restrictions"] = restrictions

            # sub classes
            subs = []
            for o in self.G.subjects(predicate=RDFS.subClassOf, object=s):
                if type(o) != BNode:
                    subs.append(str(o))
                else:
                    # sub classes collections (unionOf | intersectionOf
                    q = """
                        PREFIX owl:  <http://www.w3.org/2002/07/owl#>
                        PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>

                        SELECT ?col_type ?col_member
                        WHERE {{
                            ?sub rdfs:subClassOf <{}> .
                            ?sub owl:unionOf|owl:intersectionOf ?collection .
                            ?sub ?col_type ?collection .
                            ?collection rdf:rest*/rdf:first ?col_member .
                        }}
                    """.format(
                        s
                    )
                    collection_type = None
                    collection_members = []
                    for r in self.G.query(q):
                        collection_type = self._get_curie(str(r.col_type))
                        collection_members.append(self._get_curie(str(r.col_member)))
                    subs.append((collection_type, collection_members))
            self.CLASSES[cls]["subs"] = subs

            # retrieve the datatypes when it is a DatatypeProperty or ObjectProperty
            in_domain_of = {}
            for o in self.G.subjects(predicate=RDFS.domain, object=s):
                in_domain_of[str(o)] = {}

                #datatypes from DataProperty
                if (o, RDF.type, OWL.DatatypeProperty) in self.G:
                    ranges = self.G.objects(subject=o, predicate=RDFS.range)
                    range_list = list(ranges)
                    in_domain_of[str(o)]["datatype"] = {}
                    cnt = 0
                    if (len(range_list) >= 1):
                        for range in range_list:
                            in_domain_of[str(o)]["datatype"][cnt] = str(range)
                            cnt += 1

                #datatypes from ObjectProperty
                if (o, RDF.type, OWL.ObjectProperty) in self.G :
                    ranges = self.G.objects(subject=o, predicate=RDFS.range)
                    range_list = list(ranges)
                    cnt = 0
                    if (len(range_list) >= 1):
                        in_domain_of[str(o)]["class"] = {}
                        for range in range_list:
                            in_domain_of[str(o)]["class"][cnt] = str(range)
                            cnt += 1

            damain = in_domain_of
            #retrieve the restrictions (cardinalities and valuesets)
            for subclass in self.G.objects(subject=s, predicate=RDFS.subClassOf):
                if(subclass, RDF.type, OWL.Class) in self.G:
                    
                    # standard format: intersectionOfs
                    for intersection in self.G.objects(subject=subclass, predicate=OWL.intersectionOf):
                        collection = list(rdflibCollection.Collection(self.G, intersection))
                        for node in collection:
                            if(node, RDF.type, OWL.Restriction) in self.G:

                                # find and add cardinality restrictions
                                in_domain_of = self._add_cardinality_to_dict(in_domain_of, subclass=node)
                                # find and add valueset restrictions
                                for prop in self.G.objects(subject=node, predicate=OWL.onProperty):
                                    in_domain_of = self._retrieve_valueset_rest(in_domain_of, prop=prop, node=node)

                    # def get_path_and_values(domain, node):
                    #     for prop in list(self.G.objects(subject=node, predicate=OWL.someValuesFrom)):
                    #         print("NEw property thing: ", prop, type(prop))
                    #         if type(prop) != NodeRef:
                    #             # for prop in list(self.G.objects(subject=node, predicate=OWL.someValuesFrom)):
                    #             #     # newNode =  list(rdflibCollection.Collection(self.G, prop))
                    #             #     print(newNode)
                    #             # owl:Restriction, more than one value in owl:hasValue, path of two or three properties
                    #             if (prop, RDF.type, OWL.Class) in self.G:
                    #                 for rest in self.G.objects(subject=prop, predicate=OWL.unionOf):
                    #                     collection = list(rdflibCollection.Collection(self.G, rest))
                    #                     return {str(list(self.G.objects(subject=node, predicate=OWL.onProperty))): collection}
                    #             elif (prop, RDF.type, OWL.Restriction) in self.G:
                    #                 get_path_and_values(domain, prop)
                    #                 exit(0)
                    #             # elif (element, RDF.type, OWL.Restriction) in self.G:
                    #                 # prop_pipeline = list(self.G.objects(subject=element, predicate=OWL.onProperty))
                    #                 # for p in prop_pipeline:
                    #             # domain = get_path_and_values(domain, prop)
                    #             # exit(0)

                    # for intersection in self.G.objects(subject=subclass, predicate=OWL.intersectionOf):
                    #     collection = list(rdflibCollection.Collection(self.G, intersection))
                    #     for node in collection:
                    #         if(node, RDF.type, OWL.Restriction) in self.G:
                    #             # find and add cardinality restrictions
                    #             # damain = self._add_cardinality_to_dict(damain, subclass=node)
                    #             damain = get_path_and_values(damain, node)
                    #             print("Domain thing: ", damain)

                    # at least one prop groups (unionOf in addition to intersectionOf)
                    for union in self.G.objects(subject=subclass, predicate=OWL.unionOf):
                        collection = list(rdflibCollection.Collection(self.G, union))
                        propArray = []
                        first_prop = True

                        # append all props from one AtLeastOne group to array
                        for intersection in collection:
                            for ints in self.G.objects(subject=intersection, predicate=OWL.intersectionOf):
                                collection2 = list(rdflibCollection.Collection(self.G, ints))
                                for node in collection2:
                                    if (node , RDF.type, OWL.Restriction) in self.G:
                                        for prop in list(self.G.objects(subject=node, predicate=OWL.onProperty)):
                                            propArray.append(prop)      

                        # now iterate through atLeastOne group again
                        for intersection in collection:
                            for ints in self.G.objects(subject=intersection, predicate=OWL.intersectionOf):
                                collection2 = list(rdflibCollection.Collection(self.G, ints))
                                for node in collection2:
                                    if(node, RDF.type, OWL.Restriction) in self.G:
                                        for prop in self.G.objects(subject=node, predicate=OWL.onProperty):

                                            # add AtLeastOneWith key for identification
                                            if first_prop:
                                                propArray = list(set(propArray))
                                                propArray.remove(prop)
                                                in_domain_of[str(prop)]["atLeastOneWith"] = []
                                                in_domain_of[str(prop)]["atLeastOneWith"] = propArray
                                                first_prop = False
                                            elif prop in propArray:
                                                in_domain_of[str(prop)]["atLeastOneWith"] = []
                                                in_domain_of[str(prop)]["atLeastOneWith"].append("Yes")

                                            # find and add valueset restrictions
                                            in_domain_of = self._retrieve_valueset_rest(in_domain_of, prop=prop, node=node)

                                        # find and add cardinality restriction
                                        in_domain_of = self._add_cardinality_to_dict(in_domain_of, subclass=node, atLeastOneWith=True)

                # only one valueset and no cardinalities OR one cardinality and no other restrictions
                elif(subclass, RDF.type, OWL.Restriction) in self.G:
                    # find and add cardinality restrictions
                    in_domain_of = self._add_cardinality_to_dict(in_domain_of, subclass=subclass)
                    # find and add valueset restrictions
                    for prop in list(self.G.objects(subject=subclass, predicate=OWL.onProperty)):
                        in_domain_of = self._retrieve_valueset_rest(in_domain_of, prop=prop, node=subclass)

            # Now add in logic that if we get a restriction but it only involves sphn codes, remove the restrictions and generate a normal domain with the subset that was in the restriction instead
            # Essentially if the restriction is only on sphn codes display it in the table was the requirement, unhappy days

            for properties, domains in in_domain_of.items():
                if 'class' in domains and 'valueset' in domains:
                    if all(str(val).startswith(('https://biomedit.ch/rdf/sphn-schema/sphn#', self.METADATA['default_namespace'])) or type(val)==bool for val in domains['valueset'].values()):
                        new_class={}
                        for key in domains['valueset'].keys():
                            if str(key) == 'SPHNvalueset':
                                continue
                            new_class[key] = domains['valueset'][key]
                        domains['class'] = new_class
                        domains.pop('valueset')

            self.CLASSES[cls]["in_domain_of"] = in_domain_of


            in_domain_includes_of = []
            for o in self.G.subjects(predicate=SDO.domainIncludes, object=s):
                in_domain_includes_of.append(str(o))
            self.CLASSES[cls]["in_domain_includes_of"] = in_domain_includes_of

            in_range_of = []
            for o in self.G.subjects(predicate=RDFS.range, object=s):
                in_range_of.append(str(o))
            self.CLASSES[cls]["in_range_of"] = in_range_of

            in_range_includes_of = []
            for o in self.G.subjects(predicate=SDO.rangeIncludes, object=s):
                in_range_includes_of.append(str(o))
            self.CLASSES[cls]["in_range_includes_of"] = in_range_includes_of

            has_members = []
            for o in self.G.subjects(predicate=RDF.type, object=s):
                has_members.append(str(o))
            self.CLASSES[cls]["has_members"] = has_members

    def _extract_loops(self):
        """
        Function to automatically detect logical loops in the schema and track them.
        The current assumption is that loops are identifiable when a preddicate has the same range and domain -> hence could point to itself
        This only works for 0 Hop loops. In the code we also terminate the recursion if we find the same concept again for all the other loops

        """
        
        # SPARQL query
        query = """
        PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
        SELECT ?collision ?property
        WHERE {
            ?property a rdf:Property .
            ?property rdfs:domain ?collision .
            ?property rdfs:range ?collision . 
        }
        """

        # Execute the query
        results = self.G.query(query)
        self.LOOPS = set([(str(subject), str(object)) for subject, object in results])
        return
        
    def _add_cardinality_to_dict(self, in_domain_of, subclass, atLeastOneWith=False):

        for prop in list(self.G.objects(subject=subclass, predicate=OWL.onProperty)):
            if str(prop) not in in_domain_of :
                in_domain_of[str(prop)] = {}
            max = list(self.G.objects(subject=subclass, predicate=OWL.maxCardinality))
            if not atLeastOneWith: min = list(self.G.objects(subject=subclass, predicate=OWL.minCardinality))
            else: min = [0]

            if bool(min):
                in_domain_of[str(prop)]["minCardinality"] = {}
                in_domain_of[str(prop)]["minCardinality"] = str(min[0])

            if bool(max):
                in_domain_of[str(prop)]["maxCardinality"] = {}
                in_domain_of[str(prop)]["maxCardinality"] = str(max[0])
        
        return in_domain_of
    
    def _retrieve_valueset_rest(self, in_domain_of, prop, node):

         #check that it is not a cardinality restriction
        if bool(list(self.G.objects(subject=node, predicate=OWL.someValuesFrom))):
            in_domain_of[str(prop)]["valueset"] = {}

        # find all restrictions (different patterns), start owl:someValuesFrom
        for rest in self.G.objects(subject=node, predicate=OWL.someValuesFrom):
            # owl:Class 
            if(rest, RDF.type, OWL.Class) in self.G:
                # owl:unionOf, one or more values
                for element in self.G.objects(subject=rest, predicate=OWL.unionOf):
                    collection = list(rdflibCollection.Collection(self.G, element))
                    cnt = 0

                    for val in collection:
                        in_domain_of = self._add_valueset_to_dict(in_domain_of, prop=prop, range=val, cnt=cnt)
                        cnt += 1
                # no owl:unionOf, only one value
                if in_domain_of[str(prop)]["valueset"] == {}:
                    in_domain_of = self._add_valueset_to_dict(in_domain_of, prop=prop, range=rest, cnt=0)

            # owl:Restriction, path of more than one property
            elif(rest, RDF.type, OWL.Restriction) in self.G:
                in_domain_of[str(prop)]["valueset"]["prop_pipeline"] = {}
                prop_pipeline = list(self.G.objects(subject=rest, predicate=OWL.onProperty))

                for p in prop_pipeline:
                    in_domain_of[str(prop)]["valueset"]["prop_pipeline"][0] = str(p)

                # second owl:someValuesFrom
                for element in self.G.objects(subject=rest, predicate=OWL.someValuesFrom):
                    # owl:Class
                    if(element, RDF.type, OWL.Class) in self.G:
                        # owl:unionOf
                        for restriction in self.G.objects(subject=element, predicate=OWL.unionOf):
                            collection = list(rdflibCollection.Collection(self.G, restriction))
                            cnt = 0

                            for node in collection:
                                prop_pipeline = list(self.G.objects(subject=node, predicate=OWL.onProperty))

                                # one or more values in owl:hasValue, path of three properties
                                if bool(prop_pipeline):
                                    for p in prop_pipeline:
                                        in_domain_of[str(prop)]["valueset"]["prop_pipeline"][1] = str(p)

                                    # in_domain_of, cnt = self._get_valueset_vals(in_domain_of, prop=prop, subclass=node, predicate=OWL.hasValue, cnt=cnt)
                                    in_domain_of, cnt = self._get_valueset_vals(in_domain_of, prop=prop, subclass=node, predicate=OWL.someValuesFrom, cnt=cnt)

                                # more than one value, path of two properties
                                else:
                                    in_domain_of = self._add_valueset_to_dict(in_domain_of, prop=prop, range=node, cnt=cnt)
                                    cnt += 1

                    # owl:Restriction, more than one value in owl:hasValue, path of two or three properties
                    elif(element, RDF.type, OWL.Restriction) in self.G:
                        prop_pipeline = list(self.G.objects(subject=element, predicate=OWL.onProperty))
                        for p in prop_pipeline:
                            prop_path_ind = len(in_domain_of[str(prop)]["valueset"]["prop_pipeline"])
                            in_domain_of[str(prop)]["valueset"]["prop_pipeline"][prop_path_ind] = str(p)
                        if list(self.G.objects(element, OWL.hasValue)):
                            # in_domain_of, cnt = self._get_valueset_vals(in_domain_of, prop=prop, subclass=element, predicate=OWL.hasValue, cnt=0)
                            in_domain_of, cnt = self._get_valueset_vals(in_domain_of, prop=prop, subclass=element, predicate=OWL.hasValue, cnt=0)

                            for deeper_element in self.G.objects(subject=element, predicate=OWL.someValuesFrom):
                                for restriction in self.G.objects(subject=deeper_element, predicate=OWL.unionOf):
                                    collection = list(rdflibCollection.Collection(self.G, restriction))
                                    cnt = 0
                                    for node in collection:
                                        in_domain_of = self._add_valueset_to_dict(in_domain_of, prop=prop, range=node, cnt=cnt)
                                        cnt += 1
                        else:
                            # third owl:someValuesFrom
                            for sub_element in self.G.objects(subject=element, predicate=OWL.someValuesFrom):
                                if(sub_element, RDF.type, OWL.Class) in self.G:
                                    # owl:unionOf
                                    for sub_restriction in self.G.objects(subject=sub_element, predicate=OWL.unionOf):
                                        sub_collection = list(rdflibCollection.Collection(self.G, sub_restriction))
                                        cnt = 0

                                        for sub_node in sub_collection:
                                            prop_pipeline = list(self.G.objects(subject=sub_node, predicate=OWL.onProperty))

                                            # one or more values in owl:hasValue, path of three properties
                                            if bool(prop_pipeline):
                                                for p in prop_pipeline:
                                                    in_domain_of[str(prop)]["valueset"]["prop_pipeline"][1] = str(p)
                                                in_domain_of, cnt = self._get_valueset_vals(in_domain_of, prop=prop, subclass=sub_node, predicate=OWL.hasValue, cnt=cnt)
                                                

                                            # more than one value, path of two properties
                                            else:
                                                in_domain_of = self._add_valueset_to_dict(in_domain_of, prop=prop, range=sub_node, cnt=cnt)
                                                cnt += 1

                                # owl:Restriction, more than one value in owl:hasValue, path of two or three properties
                                elif(sub_element, RDF.type, OWL.Restriction) in self.G:
                                    prop_pipeline = list(self.G.objects(subject=sub_element, predicate=OWL.onProperty))
                                    for p in prop_pipeline:
                                        prop_path_ind = len(in_domain_of[str(prop)]["valueset"]["prop_pipeline"])
                                        in_domain_of[str(prop)]["valueset"]["prop_pipeline"][prop_path_ind] = str(p)
                                    if list(self.G.objects(sub_element, OWL.hasValue)):
                                        in_domain_of, cnt = self._get_valueset_vals(in_domain_of, prop=prop, subclass=sub_element, predicate=OWL.hasValue, cnt=0)
                                    
                                    # fourth ow:someValuesFrom
                                    elif list(self.G.objects(subject=sub_element, predicate=OWL.someValuesFrom)):
                                        for sub_sub_element in list(self.G.objects(subject=sub_element, predicate=OWL.someValuesFrom)):
                                            for sub_restriction in self.G.objects(subject=sub_sub_element, predicate=OWL.unionOf):
                                                sub_collection = list(rdflibCollection.Collection(self.G, sub_restriction))
                                                cnt = 0
                                                for some_value in sub_collection:
                                                    in_domain_of = self._add_valueset_to_dict(in_domain_of, prop=prop, range=some_value, cnt=cnt)
                                                    cnt += 1                  
                                    else:
                                        in_domain_of, cnt = self._get_valueset_vals(in_domain_of, prop=prop, subclass=sub_element, predicate=OWL.someValuesFrom, cnt=0)

                                # one value, path of two properties
                                else:
                                    in_domain_of = self._add_valueset_to_dict(in_domain_of, prop=prop, range=sub_element, cnt=0)
                    # one value, path of two properties
                    else:
                        in_domain_of = self._add_valueset_to_dict(in_domain_of, prop=prop, range=element, cnt=0)

            # more than one value in second owl:someValuesFrom
            else:
                in_domain_of, cnt = self._get_valueset_vals(in_domain_of, prop=prop, subclass=node, predicate=OWL.someValuesFrom, cnt=1)

        return in_domain_of
    
    def _get_valueset_vals(self, in_domain_of, prop, subclass, predicate, cnt):
        # find values from valueset
        vals = list(self.G.objects(subject=subclass, predicate=predicate))
        # add valueset to dict
        for range in vals:
            self._add_valueset_to_dict(in_domain_of, prop, range, cnt)
            cnt += 1
        return in_domain_of, cnt

    def _add_valueset_to_dict(self, in_domain_of, prop, range, cnt):
        # add value to dict
        in_domain_of[str(prop)]["valueset"][cnt] = str(range)
        # check if value is from snomed/chop/loinc or other
        # if any(x in str(range) for x in ["snomed", "chop", "loinc"]):
        # if ('https://biomedit.ch/rdf/sphn-schema' in str(range)):
        if not any(sub_str in str(range) for sub_str in ('http://www.w3.org/2001/XMLSchema#','https://biomedit.ch/rdf/sphn-resource/ucum/')): # Default assumption
            in_domain_of[str(prop)]["valueset"]["SPHNvalueset"] = True
        else:
            in_domain_of[str(prop)]["valueset"]["SPHNvalueset"] = False
        return in_domain_of

    def _extract_properties_uris(self):
        properties = []
        for s in self.G.subjects(predicate=RDF.type, object=RDF.Property):
            properties.append(str(s))

        for p in sorted(properties):
            self.PROPERTIES[p] = {}

    def _extract_properties(self):
        for prop in self.PROPERTIES.keys():
            s = URIRef(prop)
            # property type
            if (s, RDF.type, OWL.ObjectProperty) in self.G:
                self.PROPERTIES[prop]["prop_type"] = "op"
            elif (s, RDF.type, OWL.FunctionalProperty) in self.G:
                self.PROPERTIES[prop]["prop_type"] = "fp"
            elif (s, RDF.type, OWL.DatatypeProperty) in self.G:
                self.PROPERTIES[prop]["prop_type"] = "dp"
            elif (s, RDF.type, OWL.AnnotationProperty) in self.G:
                self.PROPERTIES[prop]["prop_type"] = "ap"
            else:
                self.PROPERTIES[prop]["prop_type"] = "p"

            self.PROPERTIES[prop]["title"] = None
            self.PROPERTIES[prop]["description"] = None
            self.PROPERTIES[prop]["definition"] = None
            self.PROPERTIES[prop]["scopeNote"] = None
            self.PROPERTIES[prop]["examples"] = []
            self.PROPERTIES[prop]["isDefinedBy"] = []
            self.PROPERTIES[prop]["source"] = None
            self.PROPERTIES[prop]["supers"] = []
            self.PROPERTIES[prop]["subs"] = []
            self.PROPERTIES[prop]["equivs"] = []
            self.PROPERTIES[prop]["invs"] = []
            self.PROPERTIES[prop]["domains"] = []
            self.PROPERTIES[prop]["domainIncludes"] = []
            self.PROPERTIES[prop]["ranges"] = []
            self.PROPERTIES[prop]["rangeIncludes"] = []

            for p, o in self.G.predicate_objects(subject=s):
                if p == DCTERMS.title:
                    self.PROPERTIES[prop]["title"] = str(o)

                if p == DCTERMS.description:
                    if self.outputformat == "md":
                        self.PROPERTIES[prop]["description"] = str(o)
                    elif self.outputformat == "adoc":
                        self.PROPERTIES[prop]["description"] = str(o)
                    else:
                        self.PROPERTIES[prop]["description"] = markdown.markdown(str(o))

                if p == SKOS.scopeNote:
                    if self.outputformat == "md":
                        self.PROPERTIES[prop]["definition"] = str(o)
                    elif self.outputformat == "adoc":
                        self.PROPERTIES[prop]["definition"] = str(o)
                    else:
                        self.PROPERTIES[prop]["definition"] = markdown.markdown(str(o))

                if p == SKOS.example:
                    self.PROPERTIES[prop]["examples"].append(self._make_example(o))

                if p == RDFS.isDefinedBy:
                    if type(o) != BNode:
                        self.PROPERTIES[prop]["isDefinedBy"].append(str(o))

                if p == DCTERMS.source or p == DC.source:
                    if str(o).startswith('http'):
                        self.PROPERTIES[prop]["source"] = self._make_formatted_uri(o)
                    else:
                        self.PROPERTIES[prop]["source"] = str(o) 
            
            # patch title from URI if we haven't got one
            if self.PROPERTIES[prop]["title"] is None:
                self.PROPERTIES[prop]["title"] = self._make_title_from_uri(prop)

            # make fid
            if "#" in prop:
                self.PROPERTIES[prop]["fid"] = prop.split("#")[1]
            else:
                self.PROPERTIES[prop]["fid"] = self._make_fid(
                    self.PROPERTIES[prop]["title"], prop
                )

            # super properties
            for o in self.G.objects(subject=s, predicate=RDFS.subPropertyOf):
                if type(o) != BNode:
                    self.PROPERTIES[prop]["supers"].append(str(o))

            # sub properties
            for o in self.G.subjects(predicate=RDFS.subPropertyOf, object=s):
                if type(o) != BNode:
                    self.PROPERTIES[prop]["subs"].append(str(o))

            # equivalent properties
            for o in self.G.objects(subject=s, predicate=OWL.equivalentProperty):
                if type(o) != BNode:
                    self.PROPERTIES[prop]["equivs"].append(str(o))

            # inverse properties
            for o in self.G.objects(subject=s, predicate=OWL.inverseOf):
                if type(o) != BNode:
                    self.PROPERTIES[prop]["invs"].append(str(o))

            # domains
            for o in self.G.objects(subject=s, predicate=RDFS.domain):
                if type(o) != BNode:
                    self.PROPERTIES[prop]["domains"].append(str(o))  # domains that are just classes
                else:
                    # domain collections (unionOf | intersectionOf
                    q = """
                        PREFIX owl:  <http://www.w3.org/2002/07/owl#>
                        PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>

                        SELECT ?col_type ?col_member
                        WHERE {{
                            <{}> rdfs:domain ?domain .
                            ?domain owl:unionOf|owl:intersectionOf ?collection .
                            ?domain ?col_type ?collection .
                            ?collection rdf:rest*/rdf:first ?col_member .
                        }}
                    """.format(
                        s
                    )
                    collection_type = None
                    collection_members = []
                    for r in self.G.query(q):
                        collection_type = self._get_curie(str(r.col_type))
                        collection_members.append(str(r.col_member))
                    self.PROPERTIES[prop]["domains"].append((collection_type, collection_members))

            # domainIncludes
            for o in self.G.objects(subject=s, predicate=SDO.domainIncludes):
                if type(o) != BNode:
                    self.PROPERTIES[prop]["domainIncludes"].append(
                        str(o)
                    )  # domainIncludes that are just classes
                else:
                    # domainIncludes collections (unionOf | intersectionOf
                    q = """
                        PREFIX owl:  <http://www.w3.org/2002/07/owl#>
                        PREFIX sdo: <https://schema.org/>

                        SELECT ?col_type ?col_member
                        WHERE {{
                            <{}> sdo:domainIncludes ?domainIncludes .
                            ?domainIncludes owl:unionOf|owl:intersectionOf ?collection .
                            ?domainIncludes ?col_type ?collection .
                            ?collection rdf:rest*/rdf:first ?col_member .
                        }}
                    """.format(
                        s
                    )
                    collection_type = None
                    collection_members = []
                    for r in self.G.query(q):
                        collection_type = self._get_curie(str(r.col_type))
                        collection_members.append(str(r.col_member))
                    self.PROPERTIES[prop]["domainIncludes"].append((collection_type, collection_members))

            # ranges
            for o in self.G.objects(subject=s, predicate=RDFS.range):
                if type(o) != BNode:
                    self.PROPERTIES[prop]["ranges"].append(str(o))
                else:
                    # range collections (unionOf | intersectionOf)
                    q = """
                        PREFIX owl:  <http://www.w3.org/2002/07/owl#>
                        PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>

                        SELECT ?col_type ?col_member
                        WHERE {{
                            <{}> rdfs:range ?range .
                            ?range owl:unionOf|owl:intersectionOf ?collection .
                            ?range ?col_type ?collection .
                            ?collection rdf:rest*/rdf:first ?col_member .
                        }}
                    """.format(
                        s
                    )
                    collection_type = None
                    collection_members = []
                    for r in self.G.query(q):
                        collection_type = self._get_curie(str(r.col_type))
                        collection_members.append(str(r.col_member))
                    self.PROPERTIES[prop]["ranges"].append((collection_type, collection_members))

            # rangeIncludes
            for o in self.G.objects(subject=s, predicate=SDO.rangeIncludes):
                if type(o) != BNode:
                    self.PROPERTIES[prop]["rangeIncludes"].append(str(o))  # rangeIncludes that are just classes
                else:
                    # rangeIncludes collections (unionOf | intersectionOf
                    q = """
                        PREFIX owl:  <http://www.w3.org/2002/07/owl#>
                        PREFIX sdo: <https://schema.org/>

                        SELECT ?col_type ?col_member
                        WHERE {{
                            <{}> sdo:rangeIncludes ?rangeIncludes .
                            ?rangeIncludes owl:unionOf|owl:intersectionOf ?collection .
                            ?rangeIncludes ?col_type ?collection .
                            ?collection rdf:rest*/rdf:first ?col_member .
                        }}
                    """.format(
                        s
                    )
                    collection_type = None
                    collection_members = []
                    for r in self.G.query(q):
                        collection_type = self._get_curie(str(r.col_type))
                        collection_members.append(str(r.col_member))
                    self.PROPERTIES[prop]["rangeIncludes"].append((collection_type, collection_members))

    def _extract_named_individuals_uris(self):
        named_individuals = []
        for s in self.G.subjects(predicate=RDF.type, object=OWL.NamedIndividual):
            named_individuals.append(str(s))

        for ni in sorted(named_individuals):
            self.NAMED_INDIVIDUALS[ni] = {}

    def _extract_named_individuals(self):
        for ni in self.NAMED_INDIVIDUALS.keys():
            if ni.startswith("http"):
                s = URIRef(ni)
            else:
                s = BNode(ni)
            # create Python dict for each NI
            self.NAMED_INDIVIDUALS[ni] = {}

            # basic NI properties
            self.NAMED_INDIVIDUALS[ni]["classes"] = set()
            self.NAMED_INDIVIDUALS[ni]["title"] = None
            self.NAMED_INDIVIDUALS[ni]["description"] = None
            self.NAMED_INDIVIDUALS[ni]["isDefinedBy"] = set()
            self.NAMED_INDIVIDUALS[ni]["source"] = None
            self.NAMED_INDIVIDUALS[ni]["seeAlso"] = None
            self.NAMED_INDIVIDUALS[ni]["sameAs"] = None

            for p, o in self.G.predicate_objects(subject=s):
                # list all the other classes of this NI
                if p == RDF.type:
                    if o != OWL.NamedIndividual:
                        self.NAMED_INDIVIDUALS[ni]["classes"].add(self._make_formatted_uri(o))

                if p == DCTERMS.title:
                    self.NAMED_INDIVIDUALS[ni]["title"] = str(o)

                if p == DCTERMS.description:
                    self.NAMED_INDIVIDUALS[ni]["description"] = str(o)

                if p == RDFS.isDefinedBy:
                    if o != OWL.NamedIndividual:
                        self.NAMED_INDIVIDUALS[ni]["isDefinedBy"].add(self._make_formatted_uri(o))

                if p == DCTERMS.source or p == DC.source:
                    if str(o).startswith('http'):
                        self.NAMED_INDIVIDUALS[ni]["source"] = self._make_formatted_uri(o)
                    else:
                        self.NAMED_INDIVIDUALS[ni]["source"] = str(o)

                if p == RDFS.seeAlso:
                    self.NAMED_INDIVIDUALS[ni]["seeAlso"] = self._make_formatted_uri(o)

                if p == OWL.sameAs:
                    self.NAMED_INDIVIDUALS[ni]["sameAs"] = self._make_formatted_uri(o)

            # patch title from URI if we haven't got one
            if self.NAMED_INDIVIDUALS[ni].get("title") is None:
                self.NAMED_INDIVIDUALS[ni]["title"] = self._make_title_from_uri(ni)

            # make fid
            if "#" in ni:
                self.NAMED_INDIVIDUALS[ni]["fid"] = ni.split("#")[1]
            else:
                self.NAMED_INDIVIDUALS[ni]["fid"] = self._make_fid(self.NAMED_INDIVIDUALS[ni]["title"], ni)
    
    def _make_sidenav(self):

        # named individuals list
        ni_fids = self._make_named_individuals(only_lists=True)

        # properties lists
        op_instances, fp_instances, dp_instances, ap_instances, p_instances = self._make_properties(only_lists=True)

        # classes list
        cls_list = self._make_classes(only_lists=True)

        # deprecated classes list
        d_cls_list = self._make_classes(only_lists=True, only_deprecated=True)

        # valuesets classes list
        valueset_list = self._make_classes(only_lists=True, only_valueset=True)

        return self._load_template("sidenav." + self.outputformat).render(
            has_classes=self.METADATA.get("has_classes"),
            has_d_classes=self.METADATA.get("has_d_classes"),
            has_valuesets=self.METADATA.get("has_valuesets"),
            has_ops=self.METADATA.get("has_ops"),
            has_fps=self.METADATA.get("has_fps"),
            has_dps=self.METADATA.get("has_dps"),
            has_aps=self.METADATA.get("has_aps"),
            has_ps=self.METADATA.get("has_ps"),
            has_nis=self.METADATA.get("has_nis"),
            ni_fids=ni_fids,
            op_instances=op_instances,
            fb_instances=fp_instances,
            dp_instances=dp_instances,
            ap_instances=ap_instances,
            p_instances=p_instances,
            cls_list=cls_list,
            d_cls_list=d_cls_list,
            valueset_list=valueset_list,
            )

    def _make_metadata(self):
        return self._load_template("metadata." + self.outputformat).render(
            imports=sorted(self.METADATA["imports"]),
            title=self.METADATA.get("title"),
            uri=self.METADATA.get("uri"),
            version_uri=self.METADATA.get("versionIRI"),
            prior_version =self.METADATA.get("priorVersion"),
            publishers=sorted(self.METADATA["publishers"]),
            creators=sorted(self.METADATA["creators"]),
            contributors=sorted(self.METADATA["contributors"]),
            created=self.METADATA.get("created"),
            modified=self.METADATA.get("modified"),
            issued=self.METADATA.get("issued"),
            source=self.METADATA.get("source"),
            description=self.METADATA.get("description"),
            historyNote=self.METADATA.get("historyNote"),
            version_info=self.METADATA.get("versionInfo"),
            license=self.METADATA.get("license"),
            rights=self.METADATA.get("rights"),
            repository=self.METADATA.get("repository"),
            ont_rdf=self._make_sphn_source_file_reference(),
            ont_rdf_ps=self._make_ps_source_file_reference(),
            has_classes=self.METADATA.get("has_classes"),
            has_d_classes=self.METADATA.get("has_d_classes"),
            has_valuesets=self.METADATA.get("has_valuesets"),
            has_ops=self.METADATA.get("has_ops"),
            has_fps=self.METADATA.get("has_fps"),
            has_dps=self.METADATA.get("has_dps"),
            has_aps=self.METADATA.get("has_aps"),
            has_ps=self.METADATA.get("has_ps"),
            has_nis=self.METADATA.get("has_nis"),
        )

    def _niceify_prefix(prefixable_iri: str) -> Union[str, str]:
        """Tries to make a simple string out of a possibly prefixable IRI

        Args:
            prefixable_iri (str): IRI to niceify

        Returns:
            Union[str, str]: pseudoprefix and concept name
        """
        if (prefixable_iri.count(":") == 1) & (prefixable_iri.count("/") == 0):
            # this is the case when we really have a prefixable iri here like sphn:Frequency
            return prefixable_iri.split(":", 1)[0], prefixable_iri.split(":", 1)[1]
        else:
            # this is the case when we just have an IRI. Then we need to reverse engineer some name for it.
            # e.g <https://biomedit.ch/rdf/sphn-ontology/psss#Microorganism>
            prefixable_iri = prefixable_iri.strip("<>")
            if '#' in str(prefixable_iri):
                iri_splitted = prefixable_iri.split("#", 1)
                pseudoprefix = iri_splitted[0].rsplit('/', 1)[1]
                name = iri_splitted[1]
            else:
                # Assumption: the second last element indicates the pseudoprefix
                iri_splitted = prefixable_iri.rsplit('/', -1)
                pseudoprefix = iri_splitted[-2]
                name = iri_splitted[-1]
            return pseudoprefix, name
        
    def _create_sparql_query(self, list_uris, classes_l, template, flat_map):
        """
        Generate unified SPARQL queries for SPHN Connector
        """
        query = "\n\nSELECT *\nWHERE\n{\n"
        prefixes = set(["PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>", 
                        "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>"])
        first = True
        for uri in list_uris:
            prefix, name = OntDoc._niceify_prefix(prefixable_iri=uri)
            if "#" in uri:
                prefixes.add(f'PREFIX {prefix}:<{uri.replace(f"#{name}", "#")}>')
            else:
                prefixes.add(f'PREFIX {prefix}:<{uri.replace(f"/{name}", "/")}>')
            class_ = classes_l[uri]
            class_template = self._load_template(template)
            concept_flat = flat_map[uri]
            concept_query = class_template.render(uri=uri,
                                                  in_domain_of=class_["in_domain_of"],
                                                  concept_flat=concept_flat,
                                                  class_name=f"{prefix}:{name}"
                                                  )
            if concept_query.strip():
                if first:
                    query += f"{{{concept_query}}}"
                    first = False
                else:
                    query = query + " UNION \n" + f"{{{concept_query}}}"
        return "\n".join(list(prefixes)) + query + "\n}"

    def _make_class(self, uri, class_, classes_l=None, template=str(), only_deprecated=False, only_valueset=False,
                    base_uri=None, base_uri_sep=None):
        if base_uri is None:
            base_uri = str(self.METADATA.get("uri"))
        if base_uri_sep is None:
            base_uri_sep = "#"
            if base_uri.endswith("#"):
                base_uri_sep = "#"
            elif base_uri.endswith("/"):
                base_uri_sep = "/"
            else:
                base_uri = base_uri + "#"

        sphn_base_uri = self.METADATA["sphn-uri"] + "#"

        # handling Markdown formatting within a table
        if self.outputformat == "md":
            class_template = self._load_template("class." + self.outputformat)
            desc = class_["description"].replace("\n", " ") if class_.get("description") is not None else None
        elif self.outputformat == "adoc":
            class_template = self._load_template("class." + self.outputformat)
            desc = class_["description"]
        elif template == "concept_flattening":
            class_template = self._load_template("class.rq")
            concept_flat = self._concept_flattening(uri, class_, classes_l)
            return class_template.render(
                base_uri=base_uri,
                base_uri_sep=base_uri_sep,
                uri=uri,
                sphn_base_uri=sphn_base_uri,
                in_domain_of=class_["in_domain_of"],
                concept_flat=concept_flat
        )
        elif template == "hasCode":
            class_template = self._load_template("class_hasCode.rq")
            concept_flat = self._concept_flattening(uri, class_, classes_l)
            
            return class_template.render(
                base_uri=base_uri,
                base_uri_sep=base_uri_sep,
                uri=uri,
                sphn_base_uri=sphn_base_uri,
                in_domain_of=class_["in_domain_of"],
                concept_flat=concept_flat
        )       
        elif template == "count_instances":
            class_template = self._load_template("class_count_instances.rq")
            concept_flat = self._concept_flattening(uri, class_, classes_l)
            return class_template.render(
                base_uri=base_uri,
                base_uri_sep=base_uri_sep,
                uri=uri,
                sphn_base_uri=sphn_base_uri,
                in_domain_of=class_["in_domain_of"],
                concept_flat=concept_flat
        ) 
        elif template == "class_predicates_min_max":
            class_template = self._load_template("class_predicates_min_max.rq")
            concept_flat = self._concept_flattening(uri, class_, classes_l)

            return class_template.render(
                base_uri=base_uri,
                base_uri_sep=base_uri_sep,
                uri=uri,
                sphn_base_uri=sphn_base_uri,
                in_domain_of=class_["in_domain_of"],
                concept_flat=concept_flat
        )      
        elif only_deprecated:
            class_template = self._load_template("deprecated_class." + self.outputformat)
            desc = class_["description"]        
        elif only_valueset:
            class_template = self._load_template("valueset." + self.outputformat)
            desc = class_["description"]
        else:
            class_template = self._load_template("class." + self.outputformat)
            desc = class_["description"]
        
        # sphn_base_uri=sphn_base_uri, ## TO DISCUSS if it should be added here to. I don't think so though
        return class_template.render(
            uri=uri,
            version_uri=OntDoc._sort_elements(self.METADATA.get("versionIRI")),
            fid=OntDoc._sort_elements(class_["fid"]),
            title=OntDoc._sort_elements(class_["title"]),
            SUBJECTTODEIDENTIFICATION=OntDoc._sort_elements(class_["SUBJECTTODEIDENTIFICATION"]) if "SUBJECTTODEIDENTIFICATION" in class_.keys() else False ,
            description=OntDoc._sort_elements(desc),
            definition=OntDoc._sort_elements(class_["definition"]),
            equivalents=OntDoc._sort_elements(class_["equivalents"]),
            supers=OntDoc._sort_elements(class_["supers"]),
            restrictions=OntDoc._sort_elements(class_["restrictions"]),
            scopeNote=OntDoc._sort_elements(class_["scopeNote"]),
            examples=OntDoc._sort_elements(class_["examples"]),
            is_defined_by=OntDoc._sort_elements(class_["isDefinedBy"]),
            source=OntDoc._sort_elements(class_["source"]),
            subs=OntDoc._sort_elements(class_["subs"]),
            in_domain_of=OntDoc._sort_elements(class_["in_domain_of"]),
            in_domain_includes_of=OntDoc._sort_elements(class_["in_domain_includes_of"]),
            in_range_of=OntDoc._sort_elements(class_["in_range_of"]),
            in_range_includes_of=OntDoc._sort_elements(class_["in_range_includes_of"]),
            has_members=OntDoc._sort_elements(class_["has_members"])
        )


    def _concept_flattening(self, uri, class_, classes_l):
        """
        Retrieves all property paths for a class that might lead to instance data
        """
        concept_flat = {}

        if not classes_l: 
            return concept_flat

        # Dynamically look for base-class and insert this instead of string look into self.METADATA.get("uri")

        # find properties that lead to classes with more properties and store full paths
        for prop in class_["in_domain_of"]:
            if (uri, self.METADATA["sphn-uri"] + prop.split('"')[1]) in self.LOOPS:
                loop = True
            else:
                loop = False
            if "class" in class_["in_domain_of"][prop]:
                concept_flat[prop] = {}
                cnt = 0
                # if property leads to more than one class, add all possibilities
                if len(class_["in_domain_of"][prop]["class"]) > 1:
                    cls = []
                    cnt_cls = 0
                    cls_not_in_classes = False
                    no_props = False
                    for k in class_["in_domain_of"][prop]["class"]:
                        if class_["in_domain_of"][prop]["class"][k].startswith("<https://"):
                            cls.append(class_["in_domain_of"][prop]["class"][k])
                        elif class_["in_domain_of"][prop]["class"][k].startswith('<a href="#'):
                            # ASSUMPTION is that 'sphn-uri' is defined
                            c = self.METADATA["sphn-uri"] + str(class_["in_domain_of"][prop]["class"][k].split('a href="')[1].split('"')[0])
                            cls.append(c)
                        elif class_["in_domain_of"][prop]["class"][k].startswith("<a href="):
                            cls.append(class_["in_domain_of"][prop]["class"][k].split('a href="')[1].split('"')[0])
                        else:
                            cls.append(uri.split("#")[0] +"#" + class_["in_domain_of"][prop]["class"][k].split("<")[1].split(">")[0])
                        if cls[cnt_cls] in classes_l:
                            if len(classes_l[cls[cnt_cls]]["in_domain_of"]) > 0:
                                for prop_next in classes_l[cls[cnt_cls]]["in_domain_of"]:
                                    if "#has" in prop_next and prop != prop_next:
                                        concept_flat[prop][cnt] = {}                
                                        concept_flat[prop][cnt]["prop"] = []
                                        concept_flat[prop][cnt]["class"] = []
                                        concept_flat[prop][cnt]["prop"].append(prop_next)
                                        concept_flat[prop][cnt]["class"].append(cls[cnt_cls])
                                        cnt+=1
                            else:
                                no_props = True
                            cnt_cls+=1
                        # for project-specific ontologies that refer direct to e.g. loinc classes 
                        else:
                            cls_not_in_classes = True
                            cnt_cls+=1
                    if cls_not_in_classes or no_props:
                        concept_flat[prop][cnt] = {}                
                        concept_flat[prop][cnt]["prop"] = []
                        concept_flat[prop][cnt]["class"] = []
                        cnt+=1
                else:
                    if class_["in_domain_of"][prop]["class"][0].startswith("<https://"):
                        cls = class_["in_domain_of"][prop]["class"][0]
                    elif class_["in_domain_of"][prop]["class"][0].startswith('<a href="#'):
                        # ASSUMPTION is that 'sphn-uri' is defined
                        cls = self.METADATA["sphn-uri"] + str(class_["in_domain_of"][prop]["class"][0].split('a href="')[1].split('"')[0])
                    elif class_["in_domain_of"][prop]["class"][0].startswith("<a href="):
                        cls = class_["in_domain_of"][prop]["class"][0].split('a href="')[1].split('"')[0]
                    else:
                        cls = uri.split("#")[0] +"#" + class_["in_domain_of"][prop]["class"][0].split("<")[1].split(">")[0]
                    if cls in classes_l:
                        if len(classes_l[cls]["in_domain_of"]) > 0:
                            for prop_next in classes_l[cls]["in_domain_of"]:
                                if "#has" in prop_next:
                                    concept_flat[prop][cnt] = {}
                                    concept_flat[prop][cnt]["prop"] = []
                                    concept_flat[prop][cnt]["class"] = []
                                    concept_flat[prop][cnt]["prop"].append(prop_next)
                                    concept_flat[prop][cnt]["class"].append(cls)
                                    cnt+=1
                        else:
                            concept_flat[prop][cnt] = {}                
                            concept_flat[prop][cnt]["prop"] = []
                            concept_flat[prop][cnt]["class"] = []
                            cnt+=1
                    # for project-specific ontologies that refer direct to e.g. loinc classes        
                    else:
                        concept_flat[prop][cnt] = {}                
                        concept_flat[prop][cnt]["prop"] = []
                        concept_flat[prop][cnt]["class"] = []
                        cnt+=1                        
        # Iterate through found paths and check if they can be extended
        for prop in concept_flat: 
            cnt = len(concept_flat[prop])
            terminated = []
            concept_flat = self._iterate_paths(uri, classes_l, concept_flat, prop, cnt, terminated)
        #get full list of paths per property in domain of, for the hasCode feature in the sparqler
        for prop in concept_flat:
            concept_flat[prop]["paths"] = {}
            for path in concept_flat[prop]:
                key = prop.split("#")[1].split('"')[0]
                if "prop" in concept_flat[prop][path]:
                    for p in concept_flat[prop][path]["prop"]:
                        key = key + p.split("#")[1].split('"')[0]
                        if p.split("#")[1].split('"')[0][-4:] == "Code":
                            break
                concept_flat[prop]["paths"][key] = True
        return concept_flat


    def _iterate_paths(self, uri, classes_l, concept_flat, prop, cnt, terminated, already_discovered=set()):
        """
        Iterates through all property paths 
        """

        ################# !!!!!! change analogous to _make_class function, ideally make the thing dynamic
        repeat = False
        concept_flat_copy = []

        # Make list of all paths to check
        for prop_next in concept_flat[prop]:
            if not concept_flat[prop][prop_next]["prop"] == []:
                concept_flat_copy.append(prop_next)
        
        # Do not check paths again that were checked in previous iteration
        if len(terminated) > 0:
            for item in terminated:
                if item in concept_flat_copy:
                    concept_flat_copy.remove(item)      
        
        # Make list of all terminated properties for next iteration
        terminated = []
        for p in concept_flat[prop]:
            terminated.append(p)

        # Create new paths out of existing path if it can be extended
        for prop_next in concept_flat_copy:
            cls = concept_flat[prop][prop_next]["class"][-1]
            property = concept_flat[prop][prop_next]["prop"][-1]
            # Breaking out of the path iteration for that property in case the Class Property combination is a loop 
            if (uri, self.METADATA["sphn-uri"] + property.split('"')[1]) in self.LOOPS:
                continue
            else:
                pass
            if "class" in classes_l[cls]["in_domain_of"][property] and str(cls) not in already_discovered:
                already_discovered.add(str(cls))
                # if property leads to more than one class, add all possibilities
                if len(classes_l[cls]["in_domain_of"][property]["class"]) > 1:
                    cls_next = []
                    cnt_len, cnt_cls = 0,0
                    for k in classes_l[cls]["in_domain_of"][property]["class"]:
                        if classes_l[cls]["in_domain_of"][property]["class"][k].startswith("<https://"):
                            cls_next.append(classes_l[cls]["in_domain_of"][property]["class"][k])
                        elif classes_l[cls]["in_domain_of"][property]["class"][k].startswith('<a href="#'):
                            # ASSUMPTION is that 'sphn-uri' is defined
                            cls_next.append(self.METADATA["sphn-uri"] + str(classes_l[cls]["in_domain_of"][property]["class"][k].split('a href="')[1].split('"')[0]))
                        elif classes_l[cls]["in_domain_of"][property]["class"][k].startswith("<a href="):
                            cls_next.append(classes_l[cls]["in_domain_of"][property]["class"][k].split('a href="')[1].split('"')[0])
                        else:
                            cls_next.append(uri.split("#")[0] +"#" + classes_l[cls]["in_domain_of"][property]["class"][k].split("<")[1].split(">")[0])
                        if cls_next[cnt_cls] in classes_l:
                            for prop_more in classes_l[cls_next[cnt_cls]]["in_domain_of"]:
                                #exclude annotation prop sphn:replaces
                                if "#has" in prop_more:
                                    repeat = True
                                    concept_flat[prop][cnt] = {}
                                    concept_flat[prop][cnt]["prop"] = []
                                    concept_flat[prop][cnt]["prop"] = concept_flat[prop][prop_next]["prop"].copy()
                                    concept_flat[prop][cnt]["prop"].append(prop_more)
                                    concept_flat[prop][cnt]["class"] = []
                                    concept_flat[prop][cnt]["class"] = concept_flat[prop][prop_next]["class"].copy()
                                    concept_flat[prop][cnt]["class"].append(cls_next[cnt_cls])
                                    cnt+=1
                            if (len(classes_l[cls_next[cnt_cls]]["in_domain_of"]) > 0):
                                cnt_len+=1
                        cnt_cls+=1
                    if cnt_len == cnt_cls:
                        concept_flat[prop].pop(prop_next)
                        terminated.remove(prop_next)
                else:
                    if classes_l[cls]["in_domain_of"][property]["class"][0].startswith("<https://"):
                        cls_next = classes_l[cls]["in_domain_of"][property]["class"][0]
                    elif classes_l[cls]["in_domain_of"][property]["class"][0].startswith('<a href="#'):
                        # ASSUMPTION is that 'sphn-uri' is defined
                        cls_next = self.METADATA["sphn-uri"] + str(classes_l[cls]["in_domain_of"][property]["class"][0].split('a href="')[1].split('"')[0])
                    elif classes_l[cls]["in_domain_of"][property]["class"][0].startswith("<a href="):
                        cls_next = classes_l[cls]["in_domain_of"][property]["class"][0].split('a href="')[1].split('"')[0]
                    else:
                        cls_next = uri.split("#")[0] +"#" + classes_l[cls]["in_domain_of"][property]["class"][0].split("<")[1].split(">")[0]
                    if cls_next in classes_l:
                        for prop_more in classes_l[cls_next]["in_domain_of"]:
                            if "#has" in prop_more:
                                repeat = True
                                concept_flat[prop][cnt] = {}
                                concept_flat[prop][cnt]["prop"] = []
                                concept_flat[prop][cnt]["prop"] = concept_flat[prop][prop_next]["prop"].copy()
                                concept_flat[prop][cnt]["prop"].append(prop_more)
                                concept_flat[prop][cnt]["class"] = []
                                concept_flat[prop][cnt]["class"] = concept_flat[prop][prop_next]["class"].copy()
                                concept_flat[prop][cnt]["class"].append(cls_next)
                                cnt+=1
                        if len(classes_l[cls_next]["in_domain_of"]) > 0:
                            concept_flat[prop].pop(prop_next)
                            terminated.remove(prop_next)
            # special case in sphn 2022: xsd:double is in Quantity, SimpleScore, TNMClassification, but with restrictions only in Quantity 
            elif "datatype" in classes_l[cls]["in_domain_of"][property]:
                if len(classes_l[cls]["in_domain_of"][property]["datatype"]) > 1:
                    hasDouble = False
                    for dt in classes_l[cls]["in_domain_of"][property]["datatype"]:
                        if (classes_l[cls]["in_domain_of"][property]["datatype"][dt].startswith("<http://www.w3.org/2001/XMLSchema#double>")
                            and cls.split("#")[1] not in ["SimpleScore", "TNMClassification"]):
                            hasDouble = True
                            concept_flat[prop][prop_next]["datatype"] = classes_l[cls]["in_domain_of"][property]["datatype"][dt]
                        elif (classes_l[cls]["in_domain_of"][property]["datatype"][dt].startswith("<http://www.w3.org/2001/XMLSchema#double>")
                            and cls.split("#")[1] in ["SimpleScore", "TNMClassification"]):
                            hasDouble = True
                            concept_flat[prop][prop_next]["datatype"] = '<http://www.w3.org/2001/XMLSchema#string><sup class="sup-c" title="class">c</sup>'
                        elif (classes_l[cls]["in_domain_of"][property]["datatype"][dt].startswith('<a href="http://www.w3.org/2001/XMLSchema#double">') 
                              and cls.split("#")[1] not in ["SimpleScore", "TNMClassification"]):
                            hasDouble = True
                            concept_flat[prop][prop_next]["datatype"] = classes_l[cls]["in_domain_of"][property]["datatype"][dt]
                        elif (classes_l[cls]["in_domain_of"][property]["datatype"][dt].startswith('<a href="http://www.w3.org/2001/XMLSchema#double">') 
                              and cls.split("#")[1] in ["SimpleScore", "TNMClassification"]):
                            hasDouble = True
                            concept_flat[prop][prop_next]["datatype"] = '<http://www.w3.org/2001/XMLSchema#string><sup class="sup-c" title="class">c</sup>'
                    if not hasDouble:
                        concept_flat[prop][prop_next]["datatype"] = classes_l[cls]["in_domain_of"][property]["datatype"][0]
                else:
                    concept_flat[prop][prop_next]["datatype"] = classes_l[cls]["in_domain_of"][property]["datatype"][0]


        # Repeat function if new paths were added
        if repeat: 
            concept_flat = self._iterate_paths( uri, classes_l, concept_flat, prop, cnt, terminated, already_discovered)

        return concept_flat


    def _has_subclasses(self, uri):
        """
        Checks for an uri whether there are any direct subclasses in the graph
        """
        subs =  self.G.subjects(RDFS.subClassOf, URIRef(uri))
        if len(list(subs)) == 0:
            return False
        else:
            return True


    def _get_subclasses_in_order(self, uri):
        """
        This routine takes an uri and returns all related subclasses as a list.
        If an element has a subclass on its own, we recursively call the function.
        """
        classes_temp = []
        classes = []
        subjects_res = self.G.subjects(RDFS.subClassOf, URIRef(uri))
        for s in list(subjects_res):
            if type(s) == BNode:
                pass
            else:
                classes_temp.append(str(s))


        for s in sorted(classes_temp):
            # if it has no subclass -> append to classes
            # if it has subclasses -> append the return
            if not self._has_subclasses(s):
                classes.append(str(s))
            else:
                classes.append(str(s))
                classes.extend(self._get_subclasses_in_order(str(s)))

        return classes


    def _make_classes(self, sparql=False, only_lists=False, only_deprecated=False, only_valueset=False):
        """
        Extended from the original version: alphabetical sorting (on the same level), deprecated classes in different section
        """ # TODO: cache query results
        # make all the individual Classes
        classes_list = []
        d_classes_list = []
        v_classes_list = []
        self.METADATA["has_d_classes"] = False
        self.METADATA["has_valuesets"] = False

        # we want to do an hierarchical sorting. So the first concept is SPHN Concept. THen all of the below ones in alphabetical order
        tempclasses_in_order = []
        d_tempclasses_in_order = []
        v_tempclasses_in_order = []
        upper_most_classes_res = self.G.query("""
                    PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
                    PREFIX owl: <http://www.w3.org/2002/07/owl#>
                    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
                    SELECT ?uri WHERE {?uri rdf:type owl:Class. FILTER NOT EXISTS {?uri rdfs:subClassOf ?otheruri} FILTER(isIRI(?uri)) }
                    """)
        upper_most_classes = sorted([str(uri.uri) for uri in upper_most_classes_res], reverse=True)
        
        all_classes = self.G.query("""
                    PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
                    PREFIX owl: <http://www.w3.org/2002/07/owl#>
                    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
                    SELECT ?uri WHERE {?uri rdf:type owl:Class.  FILTER(isIRI(?uri))}
                    """)
        all_classes = sorted([str(uri.uri) for uri in all_classes], reverse=True)

        for uri in upper_most_classes:
            # deprecated section
            if uri.endswith("Deprecated"):
                self.METADATA["has_d_classes"] = True
                d_tempclasses_in_order.append(uri)
                d_tempclasses_in_order.extend(self._get_subclasses_in_order(uri))
                # remove hierarchical ordering
                d_tempclasses_in_order.sort() # TODO: remove double sorting
            # sphn concept classes section
            else:
                tempclasses_in_order.append(uri)
                tempclasses_in_order.extend(self._get_subclasses_in_order(uri))
                tempclasses_in_order.sort()
        
        # Extend the html with all the missing classes
        tempclasses_in_order.extend(list(set(all_classes)-set(d_tempclasses_in_order)-set(tempclasses_in_order)))

        # valuesets section
        for cls in tempclasses_in_order:
            if str(cls).endswith("ValueSet"):
                self.METADATA["has_valuesets"] = True
                # add valuesets to new list
                v_tempclasses_in_order.append(str(cls))
                v_tempclasses_in_order.extend(self._get_subclasses_in_order(str(cls)))
                v_tempclasses_in_order.sort()

        complete_list_for_classindex = []
        list_for_classindex = []
        d_list_for_classindex = []
        v_list_for_classindex = []

        for tempclass in tempclasses_in_order:
            possible_match = None
            try:
                possible_match = self.CLASSES[tempclass]
            except:
                pass

            if possible_match != None:
                complete_list_for_classindex.append(tempclass)

        for tempclass in tempclasses_in_order:
            possible_match = None

            # remove valuesets from classes list  
            if tempclass not in v_tempclasses_in_order:
                try:
                    possible_match = self.CLASSES[tempclass] # TODO: Build link checks here 
                except:
                    pass

            if possible_match != None:
                list_for_classindex.append(tempclass)
                classes_list.append(self._make_class(tempclass, possible_match))

        # list deprecated classes
        for tempclass in d_tempclasses_in_order:
            possible_match = None
            try:
                possible_match = self.CLASSES[tempclass]
            except:
                pass

            if possible_match != None:
                d_list_for_classindex.append(tempclass)
                d_classes_list.append(self._make_class(tempclass, possible_match, only_deprecated=only_deprecated))

        # list valuesets classes
        for tempclass in v_tempclasses_in_order:
            possible_match = None
            try:
                possible_match = self.CLASSES[tempclass]
            except:
                pass

            if possible_match != None:
                v_list_for_classindex.append(tempclass)
                v_classes_list.append(self._make_class(tempclass, possible_match, only_valueset=only_valueset))

        # return only list of classes for outputformat rq or for didenav
        if only_lists:
            if only_deprecated:
                return d_list_for_classindex
            elif only_valueset:
                return v_list_for_classindex
            else:
                return list_for_classindex
        if sparql:
            return complete_list_for_classindex

        # make the template for all Classes
        classes_template = self._load_template("classes." + self.outputformat)
        d_classes_template = self._load_template("deprecated_classes." + self.outputformat)
        v_classes_template = self._load_template("valuesets." + self.outputformat)
        # add in Class index
        class_index = [f"<li>{self._make_formatted_uri(x)}</li>" for x in list_for_classindex]
        d_class_index = [f"<li>{self._make_formatted_uri(x)}</li>" for x in d_list_for_classindex]
        v_class_index = [f"<li>{self._make_formatted_uri(x)}</li>" for x in v_list_for_classindex]

        if only_deprecated:
            return d_classes_template.render(class_index=d_class_index, classes=d_classes_list, )
        elif only_valueset:
            return v_classes_template.render(class_index=v_class_index, classes=v_classes_list, )
        else:
            return classes_template.render(class_index=class_index, classes=classes_list, )

    def _make_property(self, property):
        # handling Markdown formatting within a table
        if self.outputformat == "md":
            desc = property[1].get("description").replace("\n", " ") \
                if property[1].get("description") is not None else None
        elif self.outputformat == "adoc":
            desc = property[1].get("description")
        else:
            desc = property[1].get("description")

        return self._load_template("property." + self.outputformat).render(
            uri=OntDoc._sort_elements(element=property[0]),
            fid=OntDoc._sort_elements(property[1].get("fid")),
            property_type=OntDoc._sort_elements(property[1].get("prop_type")),
            title=OntDoc._sort_elements(property[1].get("title")),
            SUBJECTTODEIDENTIFICATION=OntDoc._sort_elements(property[1]["SUBJECTTODEIDENTIFICATION"]) if "SUBJECTTODEIDENTIFICATION" in property[1].keys() else False ,
            description=OntDoc._sort_elements(desc),
            definition=OntDoc._sort_elements(property[1].get("definition")),
            scopeNote=OntDoc._sort_elements(property[1].get("scopeNote")),
            examples=OntDoc._sort_elements(property[1].get("examples")),
            is_defined_by=OntDoc._sort_elements(property[1]["isDefinedBy"]),
            source=OntDoc._sort_elements(property[1].get("source")),
            supers=OntDoc._sort_elements(property[1].get("supers")),
            subs=OntDoc._sort_elements(property[1].get("subs")),
            equivs=OntDoc._sort_elements(property[1].get("equivs")),
            invs=OntDoc._sort_elements(property[1].get("invs")),
            domains=OntDoc._sort_elements(property[1]["domains"]),
            domainIncludes=OntDoc._sort_elements(property[1]["domainIncludes"]),
            ranges=OntDoc._sort_elements(property[1]["ranges"]),
            rangeIncludes=OntDoc._sort_elements(property[1]["rangeIncludes"]),
        )

    def _make_properties(self, only_lists=False):
        # make all properties, grouped by OWL type
        op_instances = []
        fp_instances = []
        dp_instances = []
        ap_instances = []
        p_instances = []

        for k, v in self.PROPERTIES.items():
            if v.get("prop_type") == "op":
                op_instances.append(
                    (
                        v["title"],
                        v["fid"],
                        self._make_property((k, v)),
                    )
                )
            elif v.get("prop_type") == "fp":
                fp_instances.append(
                    (
                        v["title"],
                        v["fid"],
                        self._make_property((k, v)),
                    )
                )
            elif v.get("prop_type") == "dp":
                dp_instances.append(
                    (
                        v["title"],
                        v["fid"],
                        self._make_property((k, v)),
                    )
                )
            elif v.get("prop_type") == "ap":
                ap_instances.append(
                    (
                        v["title"],
                        v["fid"],
                        self._make_property((k, v)),
                    )
                )
            elif v.get("prop_type") == "p":
                p_instances.append(
                    (
                        v["title"],
                        v["fid"],
                        self._make_property((k, v)),
                    )
                )
        
        if only_lists:
            return op_instances, fp_instances, dp_instances, ap_instances, p_instances

        # make the template for all properties
        return self._load_template("properties." + self.outputformat).render(
            op_instances=op_instances,
            fp_instances=fp_instances,
            dp_instances=dp_instances,
            ap_instances=ap_instances,
            p_instances=p_instances,
        )

    def _make_named_individual(self, named_individual):
        return self._load_template("named_individual." + self.outputformat).render(
            uri=OntDoc._sort_elements(named_individual[0]),
            fid=OntDoc._sort_elements(named_individual[1].get("fid")),
            classes=OntDoc._sort_elements(named_individual[1].get("classes")),
            title=OntDoc._sort_elements(named_individual[1].get("title")),
            description=OntDoc._sort_elements(named_individual[1].get("description")),
            is_defined_by=OntDoc._sort_elements(named_individual[1]["isDefinedBy"]),
            source=OntDoc._sort_elements(named_individual[1].get("source")),
            see_also=OntDoc._sort_elements(named_individual[1].get("seeAlso")),
            same_as=OntDoc._sort_elements(named_individual[1].get("sameAs"))
        )

    def _make_named_individuals(self, only_lists=False):

        if not only_lists:
            named_individuals_list = []
            for k, v in self.NAMED_INDIVIDUALS.items():
                named_individuals_list.append(
                    self._make_named_individual((k, v))
                )

        # add in NIs index
        fids = []
        for k, v in self.NAMED_INDIVIDUALS.items():
            if v.get("fid") is not None:  # ensure BNodes not added
                fids.append((v.get("fid"), v.get("title")))
        fids = sorted(fids, key=lambda tup: tup[1])

        if only_lists:
            return fids

        return self._load_template("named_individuals." + self.outputformat).render(
            fids=fids,
            named_individuals=named_individuals_list
        )

    def _make_code(self, field_var) -> str:
        """Returns the given field_var as code (<code>) in this instances' output format"""
        if self.outputformat == "md":
            escaped_var = field_var.rstrip().replace("\t", "    ").split("\n")
            eg2 = ""
            for line in escaped_var:
                eg2 += f"`{line}` <br /> "
            field_var = eg2
            return field_var
        if self.outputformat == "adoc":
            return f"....\n{field_var}\n....\n\n"
        else:
            escaped_var = field_var.replace("<", "&lt;").replace(">", "&gt;")
            return f"<pre>{escaped_var}</pre>"

    def _make_resource_descriptor_example(self, rd: Union[URIRef, BNode]) -> str:
        code_formats = [
            "text/turtle",
            "text/n3",
            "application/ld+json",
            "application/json",
            "application/rdf+xml",
            "application/xml",
        ]
        markup_formats = [
            "text/html",
            "text/markdown",
            "text/asciidoc",
        ]
        is_code = False
        is_markup = False
        artifact = None
        format = None
        conforms_to = None
        for p, o in self.G.predicate_objects(subject=rd):
            if p == DCTERMS["format"]:
                format = str(o)
                if format in code_formats:
                    is_code = True
                elif format in markup_formats:
                    is_markup = True
            elif p == DCTERMS.conformsTo:
                conforms_to = str(o)
            elif p == PROF.hasArtifact:
                artifact = str(o)

        eg = ""
        if is_code:
            eg = self._make_code(artifact)
        elif is_markup:
            if format == "text/html" and self.outputformat in ["html", "md"]:
                eg = artifact
            elif format == "text/html" and self.outputformat == "adoc":
                eg = f"+++{artifact}+++\n&nbsp;"
            elif format == "text/markdown" and self.outputformat == "md":
                eg = artifact
            elif format == "text/markdown" and self.outputformat == "adoc":
                eg = f"+++{markdown.markdown(artifact)}+++\n&nbsp;"
            elif format == "text/markdown" and self.outputformat == "html":
                eg = markdown.markdown(artifact)
            elif format == "text/asciidoc" and self.outputformat == "html":
                eg = markdown.markdown(artifact)
        else:
            eg = self._make_code(artifact)

        if conforms_to is not None:
            if self.outputformat == "html":
                return f"<div style=\"border:solid 1px lightgrey; padding:5px;\">{eg}<br />Conforms to: <a href=\"{conforms_to}\">{conforms_to}</a></div>"
            elif self.outputformat == "md":
                return f"{eg}\n\nConforms to: [{conforms_to}]({conforms_to})"
            elif self.outputformat == "adoc":
                return f"{eg}\n\nConforms to: link:{conforms_to}[{conforms_to}]"
        else:
            return eg

    def _make_example(self, o: Union[URIRef, BNode, Literal]) -> str:
        """Returns an HTML / Markdown / ASCIIDOC string of this Class / Property's example, formatted according
        to this OntDoc instance's outputformat instance variable. All content needed is extracted from this
        instance's graph (self.G)"""

        o_str = str(o)

        # check to see if it is an image, if so, render it
        # could be a URIRef or Literal
        if re.findall(r"(.png|.jpg|.tiff|.gif|.webp|.pdf|.svg)#?", o_str):
            if self.outputformat == "md":
                return f"![]({o_str}) "
            if self.outputformat == "adoc":
                return f"image::{o_str}[]"
            else:
                return f"<img src=\"{o_str}\" />"

        # check to see if this is a hyperlink only, if so, render a link
        # could be a URIRef or Literal
        if re.match(r"http", o_str):
            # check to see if the hyperlink is to an object in this ont
            local = False
            for p2, o2 in self.G.predicate_objects(subject=o):
                local = True

            if local:
                return self._make_resource_descriptor_example(o)

            if self.outputformat == "md":
                return f"[{o_str}]({o_str}) "
            elif self.outputformat == "adoc":
                return f"{o_str} "
            else:
                return f"<a href=\"{o_str}\">{o_str}</a>"

        # check to see if it's a BN for further handling or a Literal
        if type(o) == BNode:
            # it must be a Resource Descriptor BN
            return self._make_resource_descriptor_example(o)
        elif type(o) == Literal:
            # handle any declared datatypes (within rdf:HTML, rdf:XMLLiteral & rdf:JSON)
            if o.datatype == RDF.HTML:
                if self.outputformat == "md":
                    return str(o)
                elif self.outputformat == "adoc":
                    return f"+++{str(o)}+++\n&nbsp;"
                else:
                    return str(o)
            if o.datatype == RDF.XMLLiteral or RDF.JSON:
                return self._make_code(o)

        # fall-back: just print out a <code>-formatted literal
        return self._make_code(o)

    def _make_document(self):
        css = None
        sparql_files = {}
        if self.outputformat == "html":
            if self.include_css:
                css = open(path.join(STYLE_DIR, "pylode.css")).read()
            # Array of templates for classes if format .rq
            sparql_files["concept_flattening"] = {}
            sparql_files["hasCode"] = {}
            sparql_files["count_instances"] = {}
            sparql_files["min_max_predicates"] = {}
            list_for_classindex = self._make_classes(sparql=True)
            uris = []
            for sparql_class in list_for_classindex:
                uri = self.METADATA.get("uri")
                uri_prefix = uri.rstrip('#').rstrip('/').split('/')[-1]
                class_prefix, _ = OntDoc._niceify_prefix(prefixable_iri=sparql_class)
                base_uri = str(self.METADATA.get("uri"))
                base_uri_sep = "#"
                if base_uri.endswith("#"):
                    base_uri_sep = "#"
                elif base_uri.endswith("/"):
                    base_uri_sep = "/"
                else:
                    base_uri = base_uri + "#"
                # Exlcude class ucum that is in the list of the SPHN 2021 version
                # Exclude classes that are not in the form https://biomedit.ch/rdf/sphn-schema/prefix#class in general (e.g. project-specific ontologies sometimes list snomed classes)
                if self.sphn_connector:
                    if class_prefix in [uri_prefix, 'sphn']:
                        uris.append(sparql_class)
                elif len(sparql_class.split("#")) == 2:
                    # only generate queries for classes of the schema
                    uri = self.METADATA.get("uri")
                    if uri.split("/")[-1] == sparql_class.split("#")[0].split("/")[-1]:
                        sparql_files["concept_flattening"][sparql_class]=self._make_class(sparql_class, self.CLASSES[sparql_class], self.CLASSES, "concept_flattening", base_uri, base_uri_sep)
                        sparql_files["hasCode"][sparql_class]=self._make_class(sparql_class, self.CLASSES[sparql_class], self.CLASSES, "hasCode", base_uri, base_uri_sep)
                        sparql_files["count_instances"][sparql_class]=self._make_class(sparql_class, self.CLASSES[sparql_class], self.CLASSES, "count_instances", base_uri, base_uri_sep)
                        sparql_files["min_max_predicates"][sparql_class]=self._make_class(sparql_class, self.CLASSES[sparql_class], self.CLASSES, "class_predicates_min_max", base_uri, base_uri_sep)
                else:
                    if sparql_class.startswith(base_uri):
                        sparql_files["concept_flattening"][sparql_class]=self._make_class(sparql_class, self.CLASSES[sparql_class], self.CLASSES, "concept_flattening", base_uri, base_uri_sep)
                        sparql_files["hasCode"][sparql_class]=self._make_class(sparql_class, self.CLASSES[sparql_class], self.CLASSES, "hasCode", base_uri, base_uri_sep)
                        sparql_files["count_instances"][sparql_class]=self._make_class(sparql_class, self.CLASSES[sparql_class], self.CLASSES, "count_instances", base_uri, base_uri_sep)
                        sparql_files["min_max_predicates"][sparql_class]=self._make_class(sparql_class, self.CLASSES[sparql_class], self.CLASSES, "class_predicates_min_max", base_uri, base_uri_sep)
                    else:
                        print("Error in the SPARQLer, not generating anything for the class with the following details:")
                        print("base_uri: " + base_uri)
                        print("base_uri_sep: " + base_uri_sep)
                        print("sparql_class: " + sparql_class)
            
            if self.sphn_connector:
                flat_map = {}
                for uri in uris:
                    flat_map[uri] = self._concept_flattening(uri, self.CLASSES[uri], self.CLASSES)
                sparql_files["count_instances"]["count-instances"] = self._create_sparql_query(uris, self.CLASSES, "class_count_instances_connector.rq", flat_map)
                sparql_files["hasCode"]["has-code"] = self._create_sparql_query(uris, self.CLASSES, "class_hasCode_connector.rq", flat_map)
                sparql_files["min_max_predicates"]["min-max-predicates-extensive"] = self._create_sparql_query(uris, self.CLASSES, "class_predicates_min_max_extensive_connector.rq", flat_map)
                sparql_files["min_max_predicates"]["min-max-predicates-fast"] = self._create_sparql_query(uris, self.CLASSES, "class_predicates_min_max_fast_connector.rq", flat_map)
        return sparql_files, self._load_template("document." + self.outputformat).render(
            schemaorg=self._make_schemaorg_metadata(),  # only does something for the HTML templates
            title=self.METADATA["title"],
            sidenav=self._make_sidenav(),
            metadata=self._make_metadata(),
            classes=self._make_classes(),
            deprecated_classes=self._make_classes(only_deprecated=True),
            valuesets=self._make_classes(only_valueset=True),
            properties=self._make_properties(),
            named_individuals=self._make_named_individuals(),
            default_namespace=self.METADATA["default_namespace"],
            namespaces=self._make_namespaces(),
            css=css,
            pylode_version=__version__
        )

    def generate_document(self):
        # expand the graph using pre-defined rules to make querying easier (poor man's inference)
        self._expand_graph()
        # get all the namespaces using several methods
        self._extract_namespaces()
        # get the default namespace
        self._get_default_namespace()
        # get the IDs (URIs) of all properties -> self.PROPERTIES
        self._extract_properties_uris()
        # get the IDs (URIs) of all classes -> CLASSES
        self._extract_classes_uris()
        # get the IDs (URIs) of all Named Individuals -> NAMED_INDIVIDUALS
        self._extract_named_individuals_uris()
        # get all the properties' details
        self._extract_properties()
        # get all the classes' details
        self._extract_classes()
        # identify loops in the Schema
        self._extract_loops()
        # get all sensitive fields
        self._extract_sensitive_fields()
        # get all the Named Individuals' details
        self._extract_named_individuals()
        # get the schema's metadata
        self._extract_metadata()
        # create fragment URIs for default namespace classes & properties
        # for each CURIE, if it's in the default namespace, i.e. this schema, use its fragment URI
        
        # crosslinking properties
        for uri, prop in self.PROPERTIES.items():
            html = []
            for p in prop["supers"]:
                prop_type = (
                    self.PROPERTIES.get(p).get("prop_type")
                    if self.PROPERTIES.get(p)
                    else None
                )
                html.append(self._make_formatted_uri(p, type=prop_type))
            self.PROPERTIES[uri]["supers"] = html

            html = []
            for p in prop["subs"]:
                prop_type = (
                    self.PROPERTIES.get(p).get("prop_type")
                    if self.PROPERTIES.get(p)
                    else None
                )
                html.append(self._make_formatted_uri(p, type=prop_type))
            self.PROPERTIES[uri]["subs"] = html

            html = []
            for p in prop["isDefinedBy"]:
                prop_type = (
                    self.PROPERTIES.get(p).get("prop_type")
                    if self.PROPERTIES.get(p)
                    else None
                )
                html.append(self._make_formatted_uri(p, type=prop_type))
            self.PROPERTIES[uri]["isDefinedBy"] = html

            html = []
            for p in prop["equivs"]:
                prop_type = (
                    self.PROPERTIES.get(p).get("prop_type")
                    if self.PROPERTIES.get(p)
                    else None
                )
                html.append(self._make_formatted_uri(p, type=prop_type))
            self.PROPERTIES[uri]["equivs"] = html

            html = []
            for p in prop["invs"]:
                prop_type = (
                    self.PROPERTIES.get(p).get("prop_type")
                    if self.PROPERTIES.get(p)
                    else None
                )
                html.append(self._make_formatted_uri(p, type=prop_type))
            self.PROPERTIES[uri]["invs"] = html

            html = []
            for d in prop["domains"]:
                if type(d) == tuple:
                    html.append(self._make_collection_class_html(d[0], d[1]))
                else:
                    html.append(self._make_formatted_uri(d, type="c"))
            self.PROPERTIES[uri]["domains"] = html

            html = []
            for d in prop["domainIncludes"]:
                if type(d) == tuple:
                    for m in d[1]:
                        html.append(self._make_formatted_uri(m, type="c"))
                else:
                    html.append(self._make_formatted_uri(d, type="c"))
            self.PROPERTIES[uri]["domainIncludes"] = html

            html = []
            for d in prop["ranges"]:
                if type(d) == tuple:
                    html.append(self._make_collection_class_html(d[0], d[1]))
                else:
                    html.append(self._make_formatted_uri(d, type="c"))
            self.PROPERTIES[uri]["ranges"] = html

            html = []
            for d in prop["rangeIncludes"]:
                if type(d) == tuple:
                    for m in d[1]:
                        html.append(self._make_formatted_uri(m, type="c"))
                else:
                    html.append(self._make_formatted_uri(d, type="c"))
            self.PROPERTIES[uri]["rangeIncludes"] = html

        # crosslinking classes
        for uri, cls in self.CLASSES.items():
            html = []
            for d in cls["equivalents"]:
                if type(d) == tuple:
                    for m in d[1]:
                        html.append(self._make_formatted_uri(m, type="c"))
                else:
                    html.append(self._make_formatted_uri(d, type="c"))
            self.CLASSES[uri]["equivalents"] = html

            html = []
            for d in cls["supers"]:
                if type(d) == tuple:
                    html.append(self._make_collection_class_html(d[0], d[1]))
                else:
                    html.append(self._make_formatted_uri(d, type="c"))
            self.CLASSES[uri]["supers"] = html

            html = []
            for d in cls["restrictions"]:
                html.append(self._make_restriction_html(uri, d))
            self.CLASSES[uri]["restrictions"] = html

            html = []
            for d in cls["subs"]:
                if type(d) == tuple:
                    for m in d[1]:
                        html.append(self._make_formatted_uri(m, type="c"))
                else:
                    html.append(self._make_formatted_uri(d, type="c"))
            self.CLASSES[uri]["subs"] = html

            html = {}
            for p in cls["in_domain_of"]:
                prop_type = (
                    self.PROPERTIES.get(p).get("prop_type")
                    if self.PROPERTIES.get(p)
                    else None
                )
                prop = self._make_formatted_uri(p, type=prop_type)
                html[prop] = {}
                for item in cls["in_domain_of"][p].items():
                    if item[0] == "datatype":
                        cnt = 0
                        html[prop]["datatype"] = {}
                        for c in item[1].items():
                            html[prop]["datatype"][cnt] = self._make_formatted_uri(c[1], type="c")
                            cnt += 1
                    if item[0] == "class":
                        cnt = 0
                        html[prop]["class"] = {}
                        for c in item[1].items():
                            html[prop]["class"][cnt] = self._make_formatted_uri(c[1], type="c")
                            cnt += 1
                    if item[0] == "minCardinality" and bool(cls["in_domain_of"][p]["minCardinality"]):
                        html[prop]["minCardinality"] = item[1]
                    if item[0] == "maxCardinality" and bool(cls["in_domain_of"][p]["maxCardinality"]):
                        html[prop]["maxCardinality"] = item[1]
                    if item[0] == "valueset" and bool(cls["in_domain_of"][p]["valueset"]):
                        cnt = 0
                        html[prop]["valueset"] = {}
                        for c in item[1].items():
                            if c[0] == "prop_pipeline":
                                n = 0
                                html[prop]["valueset"]["prop_pipeline"] = {}
                                for k in c[1].items():
                                    prop_type = (
                                        self.PROPERTIES.get(k[1]).get("prop_type")
                                        if self.PROPERTIES.get(k[1])
                                        else None
                                    )
                                    # add prop_type in case ps schema and therefore sphn:hasUnit, sphn:hasCode are added only with the label
                                    if not prop_type and ( str(k[1]).endswith("sphn#hasCode") or str(k[1]).endswith("sphn#hasUnit") ):
                                        prop_type = "op"
                                    html[prop]["valueset"]["prop_pipeline"][n] = self._make_formatted_uri(k[1], type=prop_type)
                                    n += 1
                            elif c[0] == "SPHNvalueset":
                                html[prop]["valueset"]["SPHNvalueset"] = c[1]
                            else:
                                html[prop]["valueset"][cnt] = self._make_formatted_uri(c[1], type="c")
                                cnt += 1
                for item in cls["in_domain_of"][p].items():               
                    if item[0] == "atLeastOneWith" and  bool(cls["in_domain_of"][p]["atLeastOneWith"]):
                        n = 0
                        html[prop]["atLeastOneWith"] = {}
                        for k in item[1]:
                            if k == "Yes":
                                html[prop]["atLeastOneWith"][0] = k
                            else:
                                prop_type = (
                                    self.PROPERTIES.get(str(k)).get("prop_type")
                                    if self.PROPERTIES.get(str(k))
                                    else None
                                )
                                html[prop]["atLeastOneWith"][n] = self._make_formatted_uri(str(k), type=prop_type)
                                n += 1
                        html[prop]["minCardinality"] = 0
                                
            self.CLASSES[uri]["in_domain_of"] = html

            html = []
            for p in cls["in_domain_includes_of"]:
                prop_type = (
                    self.PROPERTIES.get(p).get("prop_type")
                    if self.PROPERTIES.get(p)
                    else None
                )
                html.append(self._make_formatted_uri(p, type=prop_type))
            self.CLASSES[uri]["in_domain_includes_of"] = html

            html = []
            for p in cls["in_range_of"]:
                prop_type = (
                    self.PROPERTIES.get(p).get("prop_type")
                    if self.PROPERTIES.get(p)
                    else None
                )
                html.append(self._make_formatted_uri(p, type=prop_type))
            self.CLASSES[uri]["in_range_of"] = html

            html = []
            for p in cls["in_range_includes_of"]:
                prop_type = (
                    self.PROPERTIES.get(p).get("prop_type")
                    if self.PROPERTIES.get(p)
                    else None
                )
                html.append(self._make_formatted_uri(p, type=prop_type))
            self.CLASSES[uri]["in_range_includes_of"] = html

            html = []
            for p in cls["has_members"]:
                prop_type = (
                    self.PROPERTIES.get(p).get("prop_type")
                    if self.PROPERTIES.get(p)
                    else None
                )
                html.append(self._make_formatted_uri(p, type=prop_type))
            self.CLASSES[uri]["has_members"] = html
        return self._make_document()
    
    @staticmethod
    def _sort_elements(element):
        """Sort elements for HTML representation

        Args:
            element: element to sort
        """
        if isinstance(element, list):
            return sorted(element)
        elif isinstance(element, dict):
            return dict(sorted(element.items()))
        else:
            return element
