import rdflib
from rdflib import SKOS, URIRef, Namespace, Literal
import os
import os.path
import plac

@plac.pos('schema_prefix', "Prefix of the related schema for which the images are", type=str)
@plac.pos('image_folder_path', "Path were the images are stored in the documentation/html folder ('prefix-schema-images/year/png')", type=str)
@plac.opt('schema_version', "number of the version", type=str)

def main(schema_prefixes, image_folder_path, schema_version=None, filepath='./'):

    schema_prefixes = [('owl', rdflib.term.URIRef('http://www.w3.org/2002/07/owl#')), ('rdf', rdflib.term.URIRef('http://www.w3.org/1999/02/22-rdf-syntax-ns#')), ('rdfs', rdflib.term.URIRef('http://www.w3.org/2000/01/rdf-schema#')), ('xsd', rdflib.term.URIRef('http://www.w3.org/2001/XMLSchema#')), ('xml', rdflib.term.URIRef('http://www.w3.org/XML/1998/namespace')), ('click', rdflib.term.URIRef('https://biomedit.ch/rdf/sphn-schema/clickable#')), ('dc', rdflib.term.URIRef('http://purl.org/dc/elements/1.1/')), ('dcterms', rdflib.term.URIRef('http://purl.org/dc/terms/')), ('skos', rdflib.term.URIRef('http://www.w3.org/2004/02/skos/core#')), ('sphn', rdflib.term.URIRef('https://biomedit.ch/rdf/sphn-schema/sphn#')), ('sphn-geno', rdflib.term.URIRef('https://biomedit.ch/rdf/sphn-resource/geno/')), ('sphn-hgnc', rdflib.term.URIRef('https://biomedit.ch/rdf/sphn-resource/hgnc/')), ('sphn-so', rdflib.term.URIRef('https://biomedit.ch/rdf/sphn-resource/so/'))]

    if filepath:
        folder = filepath
    else:
        folder = "./"

    if os.path.exists(folder + image_folder_path):
        
        # # get all file names of images/concepts
        images = os.listdir(folder + image_folder_path)

        # # add prefix skos and schema_prefix
        # img_links = rdflib.Graph()
        # img_links.serialize('./onlypics.ttl' ,format='turtle')
        prefix_dict = {}
        for prefix, uri in schema_prefixes:
            # img_links.bind(prefix, Namespace(uri))
            prefix_dict[prefix] = uri

        # Create the graph
        img_links = rdflib.Graph()

        # Bind schema prefixes to the graph
        for prefix, uri in schema_prefixes:
            img_links.bind(prefix, Namespace(uri))


        # add triples with image file paths
        for img in images:
            name = img[:-4]
            prefix = name.split('_')[0]
            if prefix in prefix_dict:
                subject = URIRef(name.replace('sphn_', prefix_dict['sphn']))
                img_links.add((subject, SKOS.example, Literal(str(image_folder_path + img))))

        # save file in schema folder
        filepath = filepath  + "image_links" + ".ttl" 
        img_links.serialize(filepath ,format='turtle')
        print("file saved: ", filepath)
        return filepath
    print("all done")

# use plac
if __name__ == "__main__":
    plac.call(main)
