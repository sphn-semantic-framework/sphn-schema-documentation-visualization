import time
import requests
import json
from rdflib.graph import ConjunctiveGraph, Graph
from rdflib.plugins.sparql.processor import SPARQLResult


def runUpdate(query, db_host, name):
    if isinstance(db_host, Graph):
        t0 = time.time()
        db_host.update(query)
        t1 = time.time()
        print(f"{name}: {(t1 - t0):.2f}")
        return("Success", f"{(t1 - t0):.2f}")

    t0 = time.time()
    r = requests.post(f'{db_host}/update', data={'update': query}, auth=('admin', 'pw'))
    t1 = time.time()
    print(f"{name}: {(t1 - t0):.2f}")
    return(r.text, f"{(t1 - t0):.2f}")

def runQuery(query, db_host, name):
    if isinstance(db_host, Graph):
        t0 = time.time()
        r = db_host.query(query)
        t1 = time.time()
        print(f"{name}: {(t1 - t0):.2f}")
        if isinstance(r, SPARQLResult):
            r = r.serialize(format="json")  
        return(r, f"{(t1 - t0):.2f}")

    t0 = time.time()
    r = requests.post(f'{db_host}/query', data={'query': query}, auth=('admin', 'pw'))
    t1 = time.time()
    print(f"{name}: {(t1 - t0):.2f}")
    return(r.text, f"{(t1 - t0):.2f}")


def check_graph_labels(graph, labels, db_host):

    query = """
        PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
        PREFIX owl: <http://www.w3.org/2002/07/owl#>
        PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
        PREFIX ex: <http://example.com/>

        SELECT DISTINCT ?o
          WHERE {
            GRAPH %(project_graph)s {
          	    ?s ?p ?o .
                FILTER (
                      STRSTARTS(STR(?o), "https://www.whocc.no/atc_ddd_index/?code=") ||
                      STRSTARTS(STR(?o), "https://biomedit.ch/rdf/sphn-resource/chop/") ||
                      STRSTARTS(STR(?o), "http://purl.obolibrary.org/obo/GENO_") ||
                      STRSTARTS(STR(?o), "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/") ||
                      STRSTARTS(STR(?o), "https://biomedit.ch/rdf/sphn-resource/icd-10-gm/") ||
                      STRSTARTS(STR(?o), "https://loinc.org/rdf/") ||
                      STRSTARTS(STR(?o), "http://snomed.info/id/") ||
                      STRSTARTS(STR(?o), "http://purl.obolibrary.org/obo/SO_") ||
                      STRSTARTS(STR(?o), "https://biomedit.ch/rdf/sphn-resource/ucum/") ||
                      STRSTARTS(STR(?o), "https://biomedit.ch/rdf/sphn-resource/emdn/") ||
                      STRSTARTS(STR(?o), "http://edamontology.org/") ||
                      STRSTARTS(STR(?o), "http://www.ebi.ac.uk/efo/EFO_") ||
                      STRSTARTS(STR(?o), "http://purl.obolibrary.org/obo/GENEPIO_") ||
                      STRSTARTS(STR(?o), "http://purl.obolibrary.org/obo/OBI_") ||
                      STRSTARTS(STR(?o), "http://www.orpha.net/ORDO/Orphanet_") ||
                      STRSTARTS(STR(?o), "http://purl.obolibrary.org/obo/ECO_") 
                  )
                FILTER(?p != owl:imports)
        	}
          MINUS {
            GRAPH %(labels_graph)s {
                ?o rdfs:label ?o1
            }
          }   
        } """ % {'project_graph': graph, 'labels_graph': labels[0]}


    response = requests.post(f'{db_host}/query', data={'query': query}, auth=('admin', 'pw'))
    if response.status_code == 200:
        results = response.json()
        if results['results']['bindings']:
            raise ValueError(f"Invalid Code(s): {', '.join([r['o']['value'] for r in results['results']['bindings']])}")


def insertLabels(graph, labels, db_host, prefixes):
    """Inserts the labels of codes that are present in a preloaded graph 
    Subsequently inserts them into the named graph provided by the input

    I removed the language filter that was breaking stuff. TODO: add filter again. Currently theres no @en 
    """
    for index, label in enumerate(labels):
        if index == 0:
            Query ="""
            PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
            PREFIX owl: <http://www.w3.org/2002/07/owl#>
            PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
            PREFIX ex: <http://example.com/>
            INSERT {
                GRAPH """ + graph +  """ { ?o rdfs:label ?o1 .}
            }
            WHERE { 
                GRAPH """ + graph + """ { 
                    ?s ?p ?o 
                FILTER (
                STRSTARTS(STR(?o), "http://snomed.info/id/") ||
                STRSTARTS(STR(?o), "https://biomedit.ch/rdf/sphn-resource/chop/") ||
                STRSTARTS(STR(?o), "http://purl.obolibrary.org/obo/GENO_") ||
                STRSTARTS(STR(?o), "https://loinc.org/rdf/") ||
                STRSTARTS(STR(?o), "http://purl.obolibrary.org/obo/SO_") ||
                STRSTARTS(STR(?o), "https://biomedit.ch/rdf/sphn-resource/ucum/") ||
                STRSTARTS(STR(?o), "http://purl.obolibrary.org/obo/OBI_") ||
                STRSTARTS(STR(?o), "http://www.ebi.ac.uk/efo/EFO_") ||
                STRSTARTS(STR(?o), "http://purl.obolibrary.org/obo/ECO_") ||
                STRSTARTS(STR(?o), "https://data.jrc.ec.europa.eu/collection/ICDO3_O#") ||
                STRSTARTS(STR(?o), "https://biomedit.ch/rdf/sphn-resource/sphn/oncotree#") ||
                STRSTARTS(STR(?o), "http://edamontology.org/")
                )
                }
                GRAPH """ + label + """ { 
                    ?o rdfs:label ?o1
                }

            }
            """
            text, time = runUpdate(Query, db_host, 'insertLabels')
            assert "Success" in text, "Label insertion failed!"
        else:
            Query ="""
            PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
            PREFIX owl: <http://www.w3.org/2002/07/owl#>
            PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
            PREFIX ex: <http://example.com/>
            SELECT ?o ?o1
            WHERE { 
                {
                GRAPH """ + graph + """ { ?s ?p ?o .}
                }
                {
                GRAPH """ + label + """ { ?o rdfs:label ?o1 .}
                }
                FILTER (lang(?o1) = "" || lang(?o1) = "en") 
                BIND(CONCAT("External ", STR(?o) , " | " , ?o1, " |") AS ?newlabel)
            }
            """
            notes, time = runQuery(Query, db_host, 'insertLabels')
            notes = json.loads(notes)

            namespace_list = [(x['o']['value'], x['o1']['value']) for x in notes['results']['bindings']]
            triples = []
            for uri, label_text in namespace_list:
                if any(substring in uri for substring in prefixes.keys()):
                    for key in prefixes:
                        if key in uri:
                            prefix = prefixes[key]
                            code = uri.split(key)[-1]
                            triples.append(f'<{uri}> rdfs:label "{prefix} {code} | {label_text} |" .')
                else:
                    triples.append(f'<{uri}> rdfs:label "{label_text}" .')

            Query ="""
            PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
            PREFIX owl: <http://www.w3.org/2002/07/owl#>
            PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
            PREFIX ex: <http://example.com/>
            INSERT DATA {
                GRAPH """ + graph +  """ { """ + '\n'.join(triples) + """
                }
            }
            """
            text, time = runUpdate(Query, db_host, 'insertLabels')
            assert "Success" in text, "Label insertion failed!"
    return

def insertRecommendedLabels(graph, label_graph, db_host):
    """Inserts the labels of codes that are present in a preloaded graph 
    Subsequently inserts them into the named graph provided by the input

    I removed the language filter that was breaking stuff. TODO: add filter again. Currently theres no @en 
    """
    Query = """
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
    PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
    SELECT DISTINCT ?s ?o 
    WHERE { 
        GRAPH """ + graph +  """ {?s skos:note ?o . }
    }    
    """
    notes, time = runQuery(Query, db_host, 'insertRecommendedLabels')

    try:
        notes = json.loads(notes)
        code_list = []
        for row in notes["results"]["bindings"]:
            if 'recommended values:' not in row["o"]["value"]:
                continue
            href, elements = row["o"]["value"].split('recommended values:')
            element_list = elements.split(',')
            for part in element_list:
                code = part.split('|')[0].strip()
                if ' ' in code:
                    code = code.split(' ')[-1]
                label = part.split('|')[1].strip()
                code_list.append((code, label)) 
    except:
        code_list = []
        for row in notes:
            if 'recommended values:' not in row[1]:
                continue
            href, elements = row[1].split('recommended values:')
            element_list = elements.split(',')
            for part in element_list:
                code = part.split('|')[0].strip()
                if ' ' in code:
                    code = code.split(' ')[-1]
                label = part.split('|')[1].strip()
                code_list.append((code, label)) 
    
    for code, label in code_list:
        Query = """
        PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
        PREFIX ex: <http://example.com/>
        PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
        INSERT {
            GRAPH """ + graph +  """ { ?s rdfs:label ?o .}}
        WHERE { 
            {
            GRAPH """ + label_graph + """
                {   <http://snomed.info/id/""" + code + """> rdfs:label ?o .
                    FILTER(CONTAINS(?o, \"""" + label + """\" ))
                    BIND(<http://snomed.info/id/""" + code + """> as ?s)      
                }  
            } UNION
            {
            GRAPH """ + label_graph + """
                {   <https://biomedit.ch/rdf/sphn-resource/chop/""" + code + """> rdfs:label ?o .
                    FILTER(CONTAINS(?o, \"""" + label + """\" ))
                    BIND(<https://biomedit.ch/rdf/sphn-resource/chop/""" + code + """> as ?s)      
                }  
            } UNION
            {
            GRAPH """ + label_graph + """
                {   <http://purl.obolibrary.org/obo/GENO_""" + code + """> rdfs:label ?o .
                    FILTER(CONTAINS(?o, \"""" + label + """\" ))
                    BIND(<http://purl.obolibrary.org/obo/GENO_""" + code + """> as ?s)      
                }  
            } UNION
            {
            GRAPH """ + label_graph + """
                {   <https://loinc.org/rdf/""" + code + """> rdfs:label ?o .
                    FILTER(CONTAINS(?o, \"""" + label + """\" ))
                    BIND(<https://loinc.org/rdf/""" + code + """> as ?s)      
                }  
            }
        } 
        
        """
        text, time = runUpdate(Query, db_host, 'insertRecommendedLabels')
        assert "Success" in text, "Label insertion failed!"
    exit
    return

def transferNotes(graph, db_host):
    """
    There is this issue that a scopeNote similar to: skos:scopeNote "For sphn:hasMedicalDevice, instances of sphn:AccessDevice, sphn:LabAnalyzer, sphn:Implant are not allowed" .
    is to be displayed as a Note and not in the Restriction section, so the easy fix is to transfer those instances to skos:note which inherently 
    is displayed in the Notes section. Generally the affected notes look like: "For {}:{}, instances of {} are not allowed"
    """
    Query = """
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
    PREFIX owl: <http://www.w3.org/2002/07/owl#>
    PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
    PREFIX ex: <http://example.com/> 
    PREFIX skos: <http://www.w3.org/2004/02/skos/core#>

    INSERT {
        GRAPH """ + graph + """ {
            ?concept skos:note ?note .
        }
    }
    WHERE {
        GRAPH """ + graph + """ {
            ?concept rdf:type owl:Class .
            ?concept skos:scopeNote ?note .
            # FILTER(regex(?note, "^For\\\\s+[^,]+,\\\\s+instances\\\\s+of\\\\s+[^\\\\s]+\\\\s+are\\\\s+not\\\\s+allowed$"))
            FILTER(regex(?note, "For .*, instances of .* are not allowed"))
        }
    }
    """
    text, time = runUpdate(Query, db_host, 'transferNotes')
    assert "Success" in text, "Transferring Notes Failed!"
    return

def manualClassInferenceDomain(graph, db_host, sphn = None):
    # explicitly push inherited property down ==> But like on https://git.dcc.sib.swiss/sphn-semantic-framework/sphn-ontology-documentation-visualization/-/issues/29 
    # but only push where there is no more precise subproperty which is applicable
    """
    Produce "inference" on the schema class domains. 
    Inputs: name of Graph and sparql binary check 
    """
    if sphn is None:
        Query = """
        PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
        PREFIX owl: <http://www.w3.org/2002/07/owl#>
        PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
        PREFIX ex: <http://example.com/>
        INSERT {
            GRAPH """ + graph + """ {
                ?prop rdfs:domain ?sub .
            }
        }
        WHERE {
            GRAPH """ + graph + """ {
                ?sub rdfs:subClassOf+ ?super .
                OPTIONAL {
                    ?prop rdfs:domain ?super .
                }
                OPTIONAL {
                    ?prop rdfs:domain/owl:unionOf/rdf:rest*/rdf:first ?super .
                }
                FILTER (bound(?prop))
                FILTER NOT EXISTS {
                    ?subprop rdfs:subPropertyOf+ ?prop .
                    ?subprop rdfs:domain ?sub .
                }
            }
        }
        """
    else:
        Query = """
        PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
        PREFIX owl: <http://www.w3.org/2002/07/owl#>
        PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
        PREFIX ex: <http://example.com/>
        INSERT {
            GRAPH """ + graph + """ {
                ?prop rdfs:domain ?sub .
            }
        }
        WHERE {
            GRAPH """ + graph + """ {
                ?sub rdfs:subClassOf+ ?super .
                OPTIONAL {
                    ?prop rdfs:domain ?super .
                }
                OPTIONAL {
                    ?prop rdfs:domain/owl:unionOf/rdf:rest*/rdf:first ?super .
                }
                FILTER (bound(?prop))
                FILTER NOT EXISTS {
                    ?subprop rdfs:subPropertyOf+ ?prop .
                    ?subprop rdfs:domain ?sub .
                }
            }

            FILTER NOT EXISTS {
            GRAPH """ + sphn + """ {
            ?subprop2 rdfs:subPropertyOf+ ?prop .
            }
        }
        }
        """
    text, time = runUpdate(Query, db_host, 'manualClassInferenceDomain')
    assert "Success" in text, "Manual domain inference Failed!"
    return

def rangesListToSet(graph, db_host):
    """
    Pylode does not do lists hence the possible ranges have to be attached directly to the property node.
    This breaks the Ontology meaning but plots nicely, flattening the graph a bit
    """
    # Workaround for issue where range is a blankNode with some list TODO: fix delete ....
    Query = """
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
    PREFIX owl: <http://www.w3.org/2002/07/owl#>
    PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
    PREFIX ex: <http://example.com/>
    DELETE {
        GRAPH """ + graph + """ {
        ?property rdfs:range ?blank_node_of_union .
        }
    }
    INSERT {
        GRAPH """ + graph + """ {
        ?property rdfs:range ?o2 .
        }
    }
    WHERE {
        GRAPH """ + graph + """ {
        ?property rdfs:range ?blank_node_of_union .
        ?blank_node_of_union owl:unionOf ?o .
        ?o (rdf:rest*/rdf:first) ?o2 .
        }
    }
    """
    text, time = runUpdate(Query, db_host, 'rangesListToSet')
    assert "Success" in text, "Range to set flattening Failed!"
    return

def owl2rdfProperties(graph, db_host):
    """
    Taking a list of owl properties and adding similar rdf properties
    """
    # now also make all OWL Properties beeing also instances of RDF:Property
    Query = """
    PREFIX owl: <http://www.w3.org/2002/07/owl#>
    PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
    PREFIX ex: <http://example.com/>
    INSERT {
    GRAPH """ + graph + """ {
        ?s rdf:type rdf:Property .
        }
    }WHERE {
    GRAPH """ + graph + """ {
        ?s rdf:type ?objType .
        FILTER(?objType IN (owl:ObjectProperty, owl:FunctionalProperty, owl:DatatypeProperty, owl:AnnotationProperty))
        }
    }
    """
    text, time = runUpdate(Query, db_host, 'owl2rdfProperties')
    assert "Success" in text, "Property conversion failed!"
    return

def deleteBlankDomains(graph, db_host):
    # Delete the blank node domains
    Query = """
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
    PREFIX ex: <http://example.com/>
    DELETE {
    GRAPH """ + graph + """ {
        ?property rdfs:domain ?blank_node_of_union .
        }
    }
    WHERE {
    GRAPH """ + graph + """ {
        ?property rdfs:domain ?blank_node_of_union .
        FILTER(isblank(?blank_node_of_union))
        }
    }
    """
    text, time = runUpdate(Query, db_host, 'deleteBlankDomains')
    assert "Success" in text, "Blank node deletion Failed!"
    return

def extractPropertiesFromLocations(graph, db_host):
    """
    For the graph schema, inherit properties of parental class. However if a property exists that is a subproperty of parent 
    property, it does not get copied in.
    """
    # inherit properties of superclasses only, if there is no more specific subproperty in the class
    Query = """
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
    PREFIX owl: <http://www.w3.org/2002/07/owl#>
    PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
    PREFIX ex: <http://example.com/>
    INSERT {
    GRAPH """ + graph + """ {
        ?property rdfs:domain ?o2 .
    }
    }
    WHERE {
    {
        GRAPH ?g {
        ?property rdfs:domain ?o.
        ?o owl:unionOf/rdf:rest*/rdf:first ?o2 .
        OPTIONAL {
            ?subProperty rdfs:subPropertyOf ?property .
            ?subProperty rdfs:domain/owl:unionOf/rdf:rest*/rdf:first ?o2 .
        }
        OPTIONAL {
            ?subProperty rdfs:subPropertyOf ?property .
            ?subProperty rdfs:domain ?o2 .
        }
        OPTIONAL {
            ?o2 rdfs:subClassOf/owl:intersectionOf/rdf:rest*/rdf:first/owl:onProperty ?prop .
            FILTER(?prop = ?property)
        }
        OPTIONAL {
            ?o2 rdfs:subClassOf/owl:unionOf/rdf:rest*/rdf:first/owl:intersectionOf/rdf:rest*/rdf:first/owl:onProperty ?prop .
            FILTER(?prop = ?property)
        }
        FILTER (!bound(?subProperty) || bound(?prop))
        }
    }
    FILTER (?g in (""" + graph + """))
    }
    """
    text, time = runUpdate(Query, db_host, 'extractPropertiesFromLocations')
    assert "Success" in text, "Property propagation Failed!"
    return

def extractPropertiesFromLocationsIncludingSphn(graph, sphn, db_host):
    """
    For the graph schema, inherit properties of parental class. However if a property exists that is a subproperty of parent
    property, it does not get copied in.
    """
    # inherit properties of superclasses only, if there is no more specific subproperty in the class
    Query = """
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
    PREFIX owl: <http://www.w3.org/2002/07/owl#>
    PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
    PREFIX ex: <http://example.com/>
    PREFIX sphn: <https://biomedit.ch/rdf/sphn-schema/sphn#>
    PREFIX sph: <https://biomedit.ch/rdf/sphn-schema/sph#>
    PREFIX onto: <http://www.ontotext.com/>

    SELECT DISTINCT ?property ?o2
    FROM  """ + graph + """
    FROM  """ + sphn + """
    WHERE {
        {

        ?property rdfs:domain/(owl:unionOf/rdf:rest*/rdf:first)? ?o2 .

        OPTIONAL {
            ?subProperty rdfs:subPropertyOf ?property .
            ?subProperty rdfs:domain/owl:unionOf/rdf:rest*/rdf:first ?o2 .
        }
        OPTIONAL {
            ?subProperty rdfs:subPropertyOf ?property .
            ?subProperty rdfs:domain ?o2 .
        }
        OPTIONAL {
            ?o2 rdfs:subClassOf/owl:intersectionOf/rdf:rest*/rdf:first/owl:onProperty ?prop .
            FILTER(?prop = ?property)
        }
        OPTIONAL {
            ?o2 rdfs:subClassOf/owl:unionOf/rdf:rest*/rdf:first/owl:intersectionOf/rdf:rest*/rdf:first/owl:onProperty ?prop .
            FILTER(?prop = ?property)
                }
        FILTER (!bound(?subProperty) || bound(?prop))

    }
    }
    """
    text, time = runQuery(Query, db_host, 'extractPropertiesFromLocationsSPHN')

    tuples = [(i['property']['value'], i['o2']['value']) for i in json.loads(text)['results']['bindings'] if not i['o2']['value'].startswith('b')]
    triples = [f"<{prop}> rdfs:domain <{concept}>" for prop, concept in tuples]
    triples = ' .\n'.join(triples)

    Query = """
        PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
        PREFIX owl: <http://www.w3.org/2002/07/owl#>
        PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
        PREFIX ex: <http://example.com/>
        INSERT DATA {
        GRAPH """ + graph + """ {
            """ + triples + """
            }
        }
        """
    text, time = runUpdate(Query, db_host, 'InsertedPropertiesDomain')
    assert "Success" in text, "Properties Insertion failed"
    return

def duplicateGraph(graph, db_host):
    """
    The graph is duplicated and then its name is passed back to the main loop.
    This is currently needed to run the sparql part of the pipeline
    """
    newgraph = graph[:-1] + "_2>" if graph[-1] == ">" else graph + "_2"

    Query = """
    PREFIX ex: <http://example.com/>
    INSERT {
       GRAPH """ + newgraph + """ {
        ?s ?p ?o
       }
     }
    WHERE {
       GRAPH """ + graph + """ {
        ?s ?p ?o
       }
    }
    """
    text, time = runUpdate(Query, db_host, 'duplicateGraph')
    assert "Success" in text, "Graph duplication failed."
    return newgraph.strip("<>")

def copySphnIntoGraph(graph, sphn, db_host):
    """
    Data has to be manipulated a little differently in order for the whole graph, sphn and project
    to be displayed correctly. We just copy the two graphs together and run the pipeline again.
    """

    Query = """
    PREFIX ex: <http://example.com/>
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
    PREFIX owl: <http://www.w3.org/2002/07/owl#>
    PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
    PREFIX ex: <http://example.com/>
    PREFIX dc:   <http://purl.org/dc/elements/1.1/>
    PREFIX dcterms: <http://purl.org/dc/terms/>

    INSERT {
       GRAPH """ + graph + """ {
        ?s ?p ?o
       }
     }
    WHERE {
       GRAPH """ + sphn + """ {
        ?s ?p ?o
       }
    FILTER(
        !((?p in (rdf:type) && ?o in(owl:Ontology)) ||
        (?p in (dc:description, dc:title, dcterms:license, owl:imports, owl:versionIRI))
        ))
     }
    """
    text, time = runUpdate(Query, db_host, 'copySphnIntoGraph')
    assert "Success" in text, "Copying SPHN into project graph failed."
    return

def createSphnLabels(graph, sphn, db_host):
    """
    Bind the SPHN labels into sphn labels and add them to the graph.
    This used to be only for project specific definitions but it shouldnt matter
    
    """
    Query = """
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
    PREFIX ex: <http://example.com/>
    INSERT {
       GRAPH """ + graph + """ {
        ?s rdfs:label ?newlabel
       }
     }
    WHERE {
        GRAPH """ + sphn + """ {
        ?s rdfs:label ?label
        }
        BIND("SPHN" as ?SPHN)
        BIND(concat(?SPHN, " | " , ?label, " |") as ?newlabel)
    }
    """
    text, time = runUpdate(Query, db_host, 'createSphnLabels')
    assert "Success" in text, "Binding sphn labels successful."
    return


def add_sphn_info(graph, sphn, db_host):
    """
    Check which concepts appear in both sphn and project specific, and if there are
    overlaps, fill project gaps with sphn information
    
    It aint pretty but it is what it is
    """
    Query = """
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
    PREFIX owl: <http://www.w3.org/2002/07/owl#>
    PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
    PREFIX ex: <http://example.com/>
    INSERT {
        GRAPH """ + graph + """ {
        ?property rdf:type ?type2 .
        ?property rdf:type rdf:Property .
        ?property rdfs:domain ?s .
        ?property rdfs:range ?type .
        }
    }

    WHERE {
            {
            GRAPH """ + graph + """ { ?s ?p owl:Class .}
            }
            { 
            GRAPH """ + sphn + """{
            ?s rdfs:subClassOf ?o . 
            ?o owl:intersectionOf ?o2 .
            ?o2 (rdf:rest*/rdf:first) ?o3 .
            ?o3 owl:onProperty ?property .
            ?property rdfs:range ?type .
            ?property rdf:type ?type2 .
            FILTER (!isBlank(?type))
            }
        }
    }
    """

    Query2 = """
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
    PREFIX owl: <http://www.w3.org/2002/07/owl#>
    PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
    PREFIX ex: <http://example.com/>

    INSERT {
        GRAPH """ + graph + """ {
        ?property rdfs:range ?rangeMember .
        }
    }
    WHERE {
    GRAPH """ + graph + """  { ?s ?p owl:Class . }
    GRAPH """ + sphn + """ {
        OPTIONAL {
        ?s rdfs:subClassOf ?o .
        ?o owl:intersectionOf ?o2 .
        ?o2 (rdf:rest*/rdf:first) ?o3 .
        ?o3 owl:onProperty ?property .
        ?property rdfs:range ?type .
        FILTER (isBlank(?type)) .
        ?type owl:unionOf/rdf:rest*/rdf:first ?rangeMember .
        }
    }
    }
    """

    # this query fetches also SPHN information on properties just "used" in a project specific schema (the queries before search class based)
    # Every property that is just used in the project will have at least a rdfs:domain reference we are completing this with the rest of the information
    Query3 = """
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
    PREFIX owl: <http://www.w3.org/2002/07/owl#>
    PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
    PREFIX ex: <http://example.com/>
    INSERT {
        GRAPH """ + graph + """ {
        ?property rdf:type ?type2 .
        ?property rdf:type rdf:Property .
        ?property rdfs:range ?type .
        }
    }

    WHERE {
            {
            GRAPH """ + graph + """ { ?property rdfs:domain ?s .}
            }
            { 
            GRAPH """ + sphn + """{
                ?property rdf:type ?type2 .
                OPTIONAL { 
                    ?property rdfs:range ?type .
                    FILTER (!isBlank(?type)) .
                    }
                OPTIONAL {
                    ?property rdfs:range ?tempType .
                    FILTER (isBlank(?tempType)) .
                    ?tempType owl:unionOf/rdf:rest*/rdf:first ?type .
                    }
                }
            }
        }
    """

    text, time = runUpdate(Query, db_host, 'add_sphn_info')
    assert "Success" in text, "Error when adding in sphn ranges -- 1"
    text, time = runUpdate(Query2, db_host, 'add_sphn_info')
    assert "Success" in text, "Error when adding in sphn ranges -- 2"
    text, time = runUpdate(Query3, db_host, 'add_sphn_info')
    assert "Success" in text, "Error when adding in sphn ranges -- 3"
    return

def deleteRootsFromProperties(graph, rootNodes, db_host):
    """
    From the domains before the visualization delete the root concepts of both project and sphn
    sphn delete it by default, however for the project only delete if the domain would not be empty afterwards
    """

    Query = """
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
    PREFIX owl: <http://www.w3.org/2002/07/owl#>
    PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
    PREFIX ex: <http://example.com/>
    SELECT ?s ?o2 ?o
    WHERE {
        GRAPH """ + graph + """ {
            ?s rdfs:domain ?o .
            ?o owl:unionOf/rdf:rest*/rdf:first ?o2 .
        }
    }
    """
    text, time = runQuery(Query, db_host, 'deleteRootsFromProperties')

    # Extract the dictionary values and reorder etc.
    # very verbose
    mytouples = [(i['s']['value'], i['o2']['value']) for i in json.loads(text)['results']['bindings']]
    dictionary = {}
    to_delete = {}
    for key, value in mytouples:
        if key not in dictionary:
            dictionary[key] = [value]
        else:
            dictionary[key].append(value)
        to_delete[key] = []
    
    for key, value in dictionary.items():
        if rootNodes[1] in value: 
            value.remove(rootNodes[1])
            to_delete[key].append(rootNodes[1])

        if len(value) > 1:
            if rootNodes[0] in value: 
                value.remove(rootNodes[0])
                to_delete[key].append(rootNodes[0])

    # Proceed to do strcat to reinsert the list
    string = ""
    for key, values in to_delete.items():
        for value in values:
            string += f"<{key}>"
            string += " rdfs:domain "
            string += f"<{value}>"
            string += ".\n"

    Query = """
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
    PREFIX owl: <http://www.w3.org/2002/07/owl#>
    PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
    PREFIX ex: <http://example.com/>
    DELETE {
    GRAPH """ + graph + """ {
        """ + string + """
        }
    }
    WHERE {
        GRAPH """ + graph + """ {
        ?s rdfs:domain ?o
        }
    }
    """
    text, time = runUpdate(Query, db_host, 'deleteRootsFromProperties')

    assert "Success" in text, "Domain root deletion unsuccessful"
    return

def getRootConcepts(graph, sphn, db_host):
    """
    Delete the root concepts
    """
    if graph == sphn:
        Query = """
        PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
        PREFIX owl: <http://www.w3.org/2002/07/owl#>
        PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
        PREFIX ex: <http://example.com/>
        SELECT ?projectRoot ?sphnRoot
        WHERE {
            GRAPH """ + sphn + """ {
                ?sphnRoot rdf:type owl:Class .
            FILTER(!isBlank(?sphnRoot) && ?sphnRoot != <https://biomedit.ch/rdf/sphn-ontology/sphn#Deprecated> && ?sphnRoot != <https://biomedit.ch/rdf/sphn-schema/sphn#Deprecated>) 
            FILTER NOT EXISTS {
                ?sphnRoot rdfs:subClassOf ?q .
                }
            }
        }
        """
        text, time = runQuery(Query, db_host, 'getRootConcepts') 
        node = json.loads(text)['results']['bindings'][0]
        return ('', node['sphnRoot']['value'])
    Query = """
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
    PREFIX owl: <http://www.w3.org/2002/07/owl#>
    PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
    PREFIX ex: <http://example.com/>
    SELECT ?projectRoot ?sphnRoot
    WHERE {
        GRAPH """ + sphn + """ {
            ?sphnRoot rdf:type owl:Class .
        FILTER(!isBlank(?sphnRoot) && ?sphnRoot != <https://biomedit.ch/rdf/sphn-ontology/sphn#Deprecated> && ?sphnRoot != <https://biomedit.ch/rdf/sphn-schema/sphn#Deprecated>)
        FILTER NOT EXISTS {
            ?sphnRoot rdfs:subClassOf ?q .
            }
        }
        GRAPH """ + graph + """ {
            ?projectRoot rdf:type owl:Class .
            ?projectRoot rdfs:subClassOf ?sphnRoot .
        FILTER(!STRSTARTS(STR(?projectRoot), STRBEFORE(STR(?sphnRoot), "#")))
        }
    }
    """
    text, time = runQuery(Query, db_host, 'getRootConcepts') 
    assert json.loads(text)['results']['bindings'], "Root nodes missing"
    nodes = json.loads(text)['results']['bindings'][0]
    return (nodes['projectRoot']['value'], nodes['sphnRoot']['value'])

def remove_sphn_classes(graph, db_host):
    Query = """
        PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
        PREFIX owl: <http://www.w3.org/2002/07/owl#>
        PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
        PREFIX ex: <http://example.com/>
        DELETE {
        GRAPH """ + graph + """ {
            ?subject rdf:type owl:Class
            }
        }
        WHERE {
            GRAPH """ + graph + """ {
            ?subject rdf:type owl:Class
            FILTER(STRSTARTS(STR(?subject), 'https://biomedit.ch/rdf/sphn-schema/sphn#'))
            }
        }
        """
    text, time = runUpdate(Query, db_host, 'remove_sphn_classes')

    assert "Success" in text, "SPHN Class removal unsuccessful"
    return

def preprocessingJena(graph_name, labels, db_host, sphn=f"<https://sphn.ch/testing#sphn_rdf_schema.ttl>", prefixes=None):
    # how many of: project_specific , sphn_label_graph , class_images
    # insert labels into project here
    # Parametrize graph
    print("preprocessing Jena")
    rootNodes = getRootConcepts(graph_name, sphn, db_host)
    createSphnLabels(graph_name, sphn, db_host)
    extractPropertiesFromLocationsIncludingSphn(graph_name, sphn, db_host)
    deleteRootsFromProperties(graph_name, rootNodes, db_host)
    deleteBlankDomains(graph_name, db_host)
    owl2rdfProperties(graph_name, db_host)
    rangesListToSet(graph_name, db_host)
    manualClassInferenceDomain(graph_name, db_host, sphn)
    #check_graph_labels(graph_name, labels, db_host)
    insertLabels(graph_name, labels, db_host, prefixes)
    insertRecommendedLabels(graph_name, labels[0], db_host)
    transferNotes(graph_name, db_host)
    add_sphn_info(graph_name, sphn, db_host)
    #remove_sphn_classes(graph_name, db_host)
    # raise "Error"
    return

def preprocessingSPHNJena(graph_name, labels, db_host, prefixes=None):
    # how many of: project_specific , sphn_label_graph , class_images
    # insert labels into project here
    # Parametrize graph
    rootNodes = getRootConcepts(graph_name, graph_name, db_host)
    extractPropertiesFromLocations(graph_name, db_host)
    deleteRootsFromProperties(graph_name, rootNodes, db_host)
    deleteBlankDomains(graph_name, db_host)
    owl2rdfProperties(graph_name, db_host)
    rangesListToSet(graph_name, db_host)
    manualClassInferenceDomain(graph_name, db_host)
    insertLabels(graph_name, labels, db_host, prefixes)
    insertRecommendedLabels(graph_name, labels[0], db_host)
    transferNotes(graph_name, db_host)
    # raise "Error"
    return
