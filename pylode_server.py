# Import modules
import os
import pylode
import rdflib
from rdflib import RDFS, OWL, RDF, DC, DCTERMS, Graph, URIRef, ConjunctiveGraph
from urllib import request
import urllib
import os.path
from pathlib import Path
import time
import logging
import requests
import json
import plac
from .query_functions import preprocessingJena, copySphnIntoGraph, preprocessingSPHNJena, getRootConcepts
import traceback
import sys
sys.path.append(os.path.join(os.path.dirname(__file__), './sparqler'))
from sparqler import main as getSPARQLs

RDF_SERIALIZER_MAP = {
    "text/turtle": "turtle",
    "text/n3": "n3",
    "application/n-triples": "nt",
    "application/ld+json": "json-ld",
    "application/rdf+xml": "xml",
    # Some common but incorrect mimetypes
    "application/rdf": "xml",
    "application/rdf xml": "xml",
    "application/json": "json-ld",
    "application/ld json": "json-ld",
    "text/ttl": "turtle",
    "text/turtle;charset=UTF-8": "turtle",
    "text/ntriples": "nt",
    "text/n-triples": "nt",
    "text/plain": "nt",  # text/plain is the old/deprecated mimetype for n-triples
}

formatter = logging.Formatter('%(asctime)s %(levelname)s %(filename)s:%(lineno)s: %(message)s')
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
logger.propagate = False
consoleHandler = logging.StreamHandler()
consoleHandler.setFormatter(formatter)
logger.addHandler(consoleHandler)



@plac.opt('schema', "Schema file to load (SPHN or project-spcific)", type=str) # consider making this pos
@plac.opt('sphn_schema', "Optional schema file to load (SPHN schema if project-specific schema is provided)", type=str, abbrev='sphn') # consider making this pos
@plac.opt('db_host', "Optional db host for the distinction between running it in rdflib or with jena")
@plac.opt('image_links', "Optional image links file to load", type=str)
@plac.opt('link_rdf_schema', "Optional link to the project-specific rdf schema file", type=str)
@plac.opt('pure_sphn', "Declaration for running the sphn without a project or external terminologies", type=bool)
def main(UID='80085', schema='./mnew_concepts_proj.ttl', sphn_schema='sphn_rdf_schema.ttl', sphn_preprocessed='https://sphn.ch/testing#preprocessed_sphn_rdf_schema.ttl', db_host='http://localhost:3030/terminologies/', sparql=True, link_rdf_schema=None, image_links=None, folder='./', logfile=None, pure_sphn=False, external_terminology_file="<https://sphn.ch/testing#2025-1_external-terminologies-labels.ttl>"):
    """
    This is the entire pylode pipeline. it consists of three main parts:
    1. The Preprocessing          -- preprocessingJena
    2. The Pylode visualization   -- pylode.MakeDocco
    3. The Sparqler               -- getSPARQLs

    Most of the code in the main function deals with the standardization of runs and environments.
    """
    logger.info("SPHN only run: ")
    logger.info(pure_sphn)

    # In case no triplestore is passed, well do the whole thing by ourselves
    if db_host == "RDFLIB": 
        run_locally(UID, schema, sphn_schema, db_host, sparql, pure_sphn=pure_sphn)
        return

    db_host = db_host.rstrip('/')
    folder = folder.rstrip('/') + '/'

    if logfile:
        file_handler = logging.FileHandler(folder + logfile)
        file_handler.setLevel(logging.INFO)
        file_handler.setFormatter(formatter)
        logger.addHandler(file_handler)
    logger.info("INFO activated")

    deleteGraphs([f"<http://example.com/project_{UID}>"], db_host)
    url = f"{db_host}/data?graph=http://example.com/project_{UID}"

    logger.info("Call to: ")
    logger.info(url)    
    t0 = time.time()

    # Upload project graph
    data = open(schema).read()
    encoded_data = data.encode('utf-8')
    headers = {'Content-Type': 'text/turtle; charset=utf-8'}
    r = requests.post(url, data=encoded_data, headers=headers)
    logger.info("Uploaded " + schema + '\n' + r.text)
    t1 = time.time()    
    
    reverse_prefixes = get_reverse_prefixes(schema)

    # Uploading external ontologies and taking care of labels
    labels = [external_terminology_file]
    if os.path.exists(folder + 'ontologies'):
        for ext_ontology in os.listdir(folder + 'ontologies/'):
            data = open(folder + 'ontologies/' + ext_ontology).read()
            encoded_data = data.encode('utf-8')
            headers = {'Content-Type': 'text/turtle; charset=utf-8'}
            r = requests.post(f'{db_host}/data?graph=http://example.com/project_{UID}_{ext_ontology}', data=encoded_data, headers=headers)
            labels.append(f'<http://example.com/project_{UID}_{ext_ontology}>')
            logger.info("Uploaded " + ext_ontology + ':\n' + r.text)
    t15 = time.time()
    
    # check sphn version
    sphn_graph = Graph()
    if sphn_schema is None:
        # SPHN only case, the SPHN schema file path is passed via the 'schema' argument
        sphn_graph.parse(schema)
    else:
        # Project specific caase, the SPHN schema file path is passed via 
        sphn_graph.parse(sphn_schema)
    for _ , _, version in sphn_graph.triples((None, OWL.versionIRI, None)):
        sphn_version = str(version).split('https://biomedit.ch/rdf/sphn-schema/sphn/')[1].replace('/', '.')
        logger.info(f"SPHN version: {sphn_version}")

    if sphn_version == '2025.1':
        sphn_schema_version = '<https://sphn.ch/testing#preprocessed_sphn_rdf_schema_2025_1.ttl>'
    else:
        logger.warning(f"Not using the latest schema 2025.1, currently using legacy schema {sphn_version}")
        sphn_schema_version = '<https://sphn.ch/testing#preprocessed_sphn_rdf_schema.ttl>'
       
    # Perform pre-processing
    if pure_sphn:
        preprocessingSPHNJena(f"<http://example.com/project_{UID}>", labels, db_host, prefixes=reverse_prefixes)
    else:
        preprocessingJena(f"<http://example.com/project_{UID}>", labels, db_host, prefixes=reverse_prefixes, sphn=sphn_schema_version)

    # Add image links
    if image_links:
        data = open(image_links).read()
        encoded_data = data.encode('utf-8')
        headers = {'Content-Type': 'text/turtle; charset=utf-8'}
        r = requests.post(f'{db_host}/data?graph=http://example.com/project_{UID}', data=encoded_data, headers=headers)
        logger.info("Uploaded " + image_links + ':\n' + r.text)
    t2 = time.time()

    logger.info("moving on to Visualization")
    try:
        # Call the MakeDocco function from the pylode module TODO: Remove SPARQL generation in pylode
        s, html = pylode.MakeDocco(
            input_uri=url,
            outputformat="html",
            profile="ontdoc",
            link=link_rdf_schema
        ).document()
        t3 = time.time()
    except Exception as e:
        print('########################### ERROR ###########################')
        print(traceback.format_exc())
        print(e.message, e.args)
        exit(1)

    # Write the generated documentation to a file
    html_file = folder + schema.split('/')[-1][0:-4] + ".html"
    os.makedirs(os.path.dirname(html_file), exist_ok=True)
    file = open(html_file, "w+", encoding="utf-8")
    file.write(html)
    file.close()    
    tpres = time.time()
    logger.info("HTML: " + html_file)

    ############# New sparqler here:
    ttl_files = [Path(schema)]
    if not pure_sphn:
        ttl_files = [Path(sphn_schema), Path(schema)]

    queries_direc = folder +  "sparql-queries/" + schema.split('/')[-1][0:-4] + "/"
    queries_path = [folder +  "sparql-queries/", queries_direc]
    sub_dirs = ['concept_flattening', 'hasCode', 'min_max_predicates', 'count_instances']
    # Create the main directory if it doesn't exist
    for folder in queries_path:
        if not os.path.exists(folder):
            os.makedirs(folder)

    # Create each subdirectory if it doesn't exist
    for sub_dir in sub_dirs:
        path = os.path.join(queries_direc, sub_dir)
        if not os.path.exists(path):
            os.makedirs(path)
    
    # run SPARQler
    getSPARQLs(ttl_files, queries_direc)
    tposts = time.time()

    to_be_deleted = [f"<http://example.com/project_{UID}>"]
    if len(labels) > 1:
        to_be_deleted.append(labels[1:])

    deleteGraphs(to_be_deleted, db_host)

    t4 = time.time()
    elapsed_time = t4 - t0
    pylo = t3 - t2
    ut = t2 - t1
    sparql_time = tposts - tpres

    logger.info(f"Preproc time: {ut:.2f} seconds")
    logger.info(f"Pylode time: {pylo:.2f} seconds")
    if sparql: logger.info(f"SPARQL time: {sparql_time:.2f} seconds")
    logger.info(f"Total time: {elapsed_time:.2f} seconds")
    logger.info("done")
    return

def deleteGraphs(list, db_host):
    Query = ""
    for element in list:
        Query += f"DROP GRAPH {element} ; \n"
    r = requests.post(f'{db_host}/update', data={'update': Query}, auth=('admin', 'pw'))

def get_reverse_prefixes(schema):
    # Generate prefix dictionary, used for label insertion in preprocessing
    graph = Graph()
    graph.parse(schema)
    namespace_list = [(i,j) for i,j in graph.namespaces()]
    reverse_prefixes = {}
    for prefix, uri in namespace_list:
        reverse_prefixes[uri] = prefix
    return reverse_prefixes


def parse_locally(schema, sphn_schema, labels, pure_sphn, mode='connector'):
    """
    - Instantiate the Graphs, sphn, labels, and project graph.
    - Run preprocessing against the graphs
    - Save everything locally or smth i dont even know
    """
    g = ConjunctiveGraph()


    t0 = time.time()
    project_name = URIRef(f"http://example.com/project_8008135")
    sphn_name = URIRef(f"http://example.com/sphn_8008135")
    labels_name = URIRef(f"http://example.com/labels_8008135")

    g.parse(schema, format="turtle", publicID=project_name)
    if not pure_sphn:
        g.parse(sphn_schema, format="turtle", publicID=sphn_name)
    g.parse(labels, format="turtle", publicID=labels_name)

    t1 = time.time()

    return g, project_name, sphn_name, labels_name

def run_locally(UID='80085', schema='./SPHN_dataset_template_2023_3_with_external_ordo.ttl', sphn_schema='./sphn_rdf_schema.ttl', db_host='RDFLIB_DEFAULT', sparql=True, link_rdf_schema=None, folder='./', pure_sphn=False, mode='Connector'):
    if not pure_sphn:
        assert sphn_schema != 'https://sphn.ch/testing#sphn_rdf_schema.ttl', "Please feed me with an sphn_schema"

    # Generate prefix dictionary, used for label insertion in preprocessing
    graph = Graph()
    graph.parse(schema)
    namespace_list = [(i,j) for i,j in graph.namespaces()]
    reverse_prefixes = {}
    for prefix, uri in namespace_list:
        reverse_prefixes[uri] = prefix

    g, project, sphn, labels = parse_locally(schema=schema, sphn_schema=sphn_schema, labels='./2024-2_external-terminologies-labels.ttl', pure_sphn=pure_sphn)

    if pure_sphn:
        preprocessingSPHNJena(f"<{project}>", [f"<{labels}>"], g, reverse_prefixes)
    else:
        preprocessingJena(f"<{project}>", [f"<{labels}>"], g, sphn=f"<{sphn}>", prefixes=reverse_prefixes)

    project_graph = g.get_context(URIRef(project))

    # Call the MakeDocco function from the pylode module
    sparql_files, html = pylode.MakeDocco(
        data=project_graph,
        outputformat="html",
        profile="ontdoc",
        link=link_rdf_schema
    ).document()
    t3 = time.time()

    # Write the generated documentation to a file
    html_file = './' + schema.split('/')[-1][0:-4] + ".html"


    os.makedirs(os.path.dirname(html_file), exist_ok=True)
    file = open(html_file, "w+", encoding="utf-8")
    file.write(html)
    file.close()

    logger.info("moving on to Sparql")
    ############# New sparqler here:
    ttl_files = [Path(schema)]
    if not pure_sphn:
        ttl_files = [Path(sphn_schema), Path(schema)]

    queries_direc = folder +  "sparql-queries/" + schema.split('/')[-1][0:-4] + "/"
    queries_path = [folder +  "sparql-queries/", queries_direc]
    sub_dirs = ['concept_flattening', 'hasCode', 'min_max_predicates', 'count_instances']
    # Create the main directory if it doesn't exist
    for folder in queries_path:
        if not os.path.exists(folder):
            os.makedirs(folder)

    # Create each subdirectory if it doesn't exist
    for sub_dir in sub_dirs:
        path = os.path.join(queries_direc, sub_dir)
        if not os.path.exists(path):
            os.makedirs(path)
    
    getSPARQLs(ttl_files, queries_direc)
    return

# use plac
if __name__ == "__main__":
    # main()
    plac.call(main)