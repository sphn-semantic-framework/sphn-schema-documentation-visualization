# Import modules
import rdflib
from rdflib import RDFS, OWL, RDF, DC, DCTERMS, Graph, URIRef, ConjunctiveGraph, RDF
import os
import time
import logging
import json
import plac
from jinja2 import Environment, FileSystemLoader
from rml_generator_lib import get_restrictions, get_restrictions_map, simplify_schema_graph, niceify_prefix
from pathlib import Path

script_dir = os.path.dirname(os.path.abspath(__file__))

global_path_list = []
global_prefixes = []
object_property_exceptions = [URIRef('https://biomedit.ch/rdf/sphn-schema/sphn#hasPredecessor')]
rootNodes = ('https://biomedit.ch/rdf/sphn-schema/genotech#whatever', 'https://biomedit.ch/rdf/sphn-schema/sphn#SPHNConcept')

def get_uri(rootNodes=rootNodes):
    sphn = rootNodes[0]
    proj_pre = 'someDefault'
    if len(rootNodes) >= 2:
        proj = rootNodes[1]
        if '#' in proj:
            proj_pre = proj.split('#')[0]
        else:
            proj_pre = '/'.join(proj.split('/')[:-1])
    return (proj_pre, sphn.split('#')[0])

def extract_concept_name(concept):
    if '/sphn#' in concept:
        return concept.split('#')[-1]
    elif '/' in concept:
        return concept.split('/')[-1]
    else:
        raise ValueError("Concept definition unknown")

def remove_exact_string(original_string, string_to_remove):
    if original_string.startswith(string_to_remove):
        return original_string[len(string_to_remove):]
    return original_string

def getRestrictions(Graph, classes, source='restrictions_map.json'):
    res = get_restrictions(Graph, classes)
    res = get_restrictions_map(res)
    return res

def load_graph(paths, schema_file_type='turtle'):
    """Loads schema graph into memory

    Args:
        path (Path): local path to graph file
        schema_file_type (str): type of file, e.g. 'turtle'

    Returns:
        Graph: Graph object representing the loaded graph
    """
    # print(f"Loading schema files: {paths}")
    combined_graph = Graph()

    for path in paths:
        if not isinstance(path, Path): #Currently the graphs are only passed as files
            msg = f"Graph in Memory"
            logging.warning(msg)
            try:
                combined_graph.parse(data=path, format=schema_file_type)
            except Exception as e:
                error_msg = f"Parsing of memory file failed, length: {len(path)}, {e}"
                logging.error(error_msg)
                raise Exception(error_msg)
        else:
            try:
                logging.debug(f"Trying to parse: {path} in format {schema_file_type}")
                combined_graph.parse(str(path), format=schema_file_type)
            except Exception as e:
                error_msg = f"Parsing of file '{path}' failed: {e}"
                logging.error(error_msg)
                raise Exception(error_msg)

    return combined_graph

def extractConcepts(g):
    concepts = {}
    deprecated_uri = URIRef("https://biomedit.ch/rdf/sphn-schema/sphn#Deprecated")

    all_concepts = [c for c in g.subjects(RDF.type, OWL.Class)
                    if not type(c) is rdflib.term.BNode]
    deprecated_concepts = [x for x in (g.subjects(RDFS.subClassOf, deprecated_uri)) if type(x)==URIRef] + [deprecated_uri]

    for concept in all_concepts:
        if concept not in deprecated_concepts:
            properties = list(g.subjects(RDFS.domain, concept))
            concepts[concept] = {'properties': properties}
    return concepts

def extractProperties(g):
    DatatypeProperties = {}
    ObjectProperties = {}

    datatypes = set(list(g.subjects(RDF.type, OWL.DatatypeProperty)))
    objects = list(g.subjects(RDF.type, OWL.ObjectProperty))

    for prop in objects:
        ranges = list(g.objects(prop, RDFS.range))
        ObjectProperties[prop] = {'ranges': ranges}

    for data in datatypes:
        ranges = list(g.objects(data, RDFS.range))
        DatatypeProperties[data] = {'ranges': ranges}
    return DatatypeProperties, ObjectProperties


def preformatting(path):
    path = str(path)
    if 'https://biomedit.ch/rdf/sphn-schema/sphn#' in path:
        return path.replace('https://biomedit.ch/rdf/sphn-schema/sphn#', '/sphn:')
    elif 'https://biomedit.ch/rdf/sphn-ontology/sphn#' in path:
        return path.replace('https://biomedit.ch/rdf/sphn-ontology/sphn#', '/sphn:')
    else:
        return '/<{}>'.format(path)

def DFS_Flattening(Prefixes, Concepts, DatatypeProperties, ObjectProperties, Restrictions, current, path, seenConcepts):
    new_seen = seenConcepts + [current]
    if not current in Concepts:
        return
    if not Concepts[current]['properties']:
        pass
    for prop in Concepts[current]['properties']:
        mypath = path + preformatting(prop)
        global_path_list.append(mypath[1:])
        if prop in DatatypeProperties:
            pass
        # Checking for Restrictions on the Project
        elif str(current) + str(prop) in Restrictions and all(item.startswith(Prefixes[0]) for item in Restrictions[str(current) + str(prop)]):
            for concept in Restrictions[str(current) + str(prop)]:
                if URIRef(concept) in new_seen:
                    continue
                DFS_Flattening(Prefixes, Concepts, DatatypeProperties, ObjectProperties, Restrictions, URIRef(concept), mypath, new_seen)
        # Analogously checking for Restrictions on SPHN concepts
        elif str(current) + str(prop) in Restrictions and all(item.startswith(Prefixes[1]) for item in Restrictions[str(current) + str(prop)]):
            for concept in Restrictions[str(current) + str(prop)]:
                if URIRef(concept) in new_seen:
                    continue
                DFS_Flattening(Prefixes, Concepts, DatatypeProperties, ObjectProperties, Restrictions, URIRef(concept), mypath, new_seen)
        else:
            for concept in ObjectProperties[prop]['ranges']:
                if concept in new_seen:
                    continue
                DFS_Flattening(Prefixes, Concepts, DatatypeProperties, ObjectProperties, Restrictions, concept, mypath, new_seen)
    return 'Valueset'

def render_flattening(Concepts, DatatypeProperties, ObjectProperties, Restrictions, output):
    global global_prefixes
    os.makedirs(os.path.join(output, 'concept_flattening'), exist_ok=True)
    for concept in Concepts.keys():
        global global_path_list

        global_path_list = []
        DFS_Flattening(global_prefixes, Concepts, DatatypeProperties, ObjectProperties, Restrictions, concept, '', [])
        paths = global_path_list

        env = Environment(loader=FileSystemLoader(script_dir))  # '.' means current directory
        template = env.get_template('flattening.rq')   # Name of your template file
        lines = []
        for path in paths:
            element = path.replace(global_prefixes[0], '')
            element = element.replace('sphn:has', '')
            element = element.replace('<', '')
            element = element.replace('>', '')
            element = element.replace('#', '')
            element = element.replace('/', '_')
            lines.append(str(path) + ' ?' + element)


        rendered = template.render(Concept=concept, paths=lines)

        file_path = os.path.join(output, 'concept_flattening', extract_concept_name(concept) + '.rq')
        with open(file_path, 'w') as file:
            file.write(rendered)
    return 'Finished'

def render_count(Concepts, DatatypeProperties, ObjectProperties, Restrictions, output, sphn_namespace, sphn_connector = False):
    global global_prefixes
    os.makedirs(os.path.join(output, 'count_instances'), exist_ok=True)
    env = Environment(loader=FileSystemLoader(script_dir))

    if sphn_connector:
        template = env.get_template('count_connector.rq')
        file_path = os.path.join(output, 'count_instances/count-instances.rq')
        file = open(file_path, 'w')
        file.write(f"PREFIX sphn:<{sphn_namespace}#>\nPREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\nPREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>\nSELECT *\nWHERE\n{{\n")
    else:
        template = env.get_template('count.rq')
    
    for i_concept, concept in enumerate(sorted(Concepts.keys()), start=0):
        global global_path_list
        global_path_list = []
        if sphn_connector:
            concept_prefix, concept_name = niceify_prefix(prefixable_iri=concept)
            concept_str = f"{concept_prefix}:{concept_name}"
        DFS_Flattening(global_prefixes, Concepts, DatatypeProperties, ObjectProperties, Restrictions, concept, '', [])

        # Here do some exclusions if you need but dont you dare touch my DFS
        lines = []
        for path in global_path_list:
            lines.append(str(path))

        if sphn_connector:
            rendered = template.render(Concept=concept, paths=lines, ConceptStr=concept_str)
            if i_concept == 0:
                file.write(f"{{{rendered}}}")
            else:
                file.write(" UNION \n" + f"{{{rendered}}}")
        else:
            rendered = template.render(Concept=concept, paths=lines)
            file_path = os.path.join(output, 'count_instances', extract_concept_name(concept) + '.rq')
            with open(file_path, 'w') as file:
                file.write(rendered)
    
    if sphn_connector:
        file.write('\n}')
        file.close()

    return 'Finished'

def write_min_max_queries(file: object, template: object, concept: str, lines: list, concept_str: str, first: bool):
    """Write Connector queries
    """
    rendered = template.render(Concept=concept, paths=sorted(lines), ConceptStr=concept_str)
    if 'SELECT ?concept ?origin' not in rendered:
        return first
    if first:
        file.write(f"{{{rendered}}}")
        first = False
    else:
        file.write(" UNION \n" + f"{rendered}")
    return first

def render_min_max(Concepts, DatatypeProperties, ObjectProperties, Restrictions, output, sphn_namespace, sphn_connector = False):
    global global_prefixes
    env = Environment(loader=FileSystemLoader(script_dir))
    os.makedirs(os.path.join(output, 'min_max_predicates'), exist_ok=True)

    if sphn_connector:
        template_fast = env.get_template('min_max_fast_connector.rq')
        template_ext = env.get_template('min_max_extensive_connector.rq')
        file_path_fast = os.path.join(output, 'min_max_predicates/min-max-predicates-fast.rq')
        file_path_ext = os.path.join(output, 'min_max_predicates/min-max-predicates-extensive.rq')
        file_fast = open(file_path_fast, 'w')
        file_ext= open(file_path_ext, 'w')
        prefixes = f"PREFIX sphn:<{sphn_namespace}#>\nPREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\nPREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>\nSELECT *\nWHERE\n{{\n"
        file_fast.write(prefixes)
        file_ext.write(prefixes)
    else:
        template = env.get_template('min_max.rq')

    first_fast = True
    first_ext = True
    for concept in sorted(Concepts.keys()):
        global global_path_list
        global_path_list = []
        if sphn_connector:
            concept_prefix, concept_name = niceify_prefix(prefixable_iri=concept)
            concept_str = f"{concept_prefix}:{concept_name}"
        DFS_Flattening(global_prefixes, Concepts, DatatypeProperties, ObjectProperties, Restrictions, concept, '', [])
        # Here do some exclusions if you need but dont you dare touch my DFS
        # For performance Improvement only run the DFS once and then pass the results to the render funtions
        lines = []
        for path in global_path_list:
            lastPath = URIRef(path.split('/')[-1].replace('sphn:', f'{sphn_namespace}#'))
            if lastPath in DatatypeProperties:
                for dType in DatatypeProperties[lastPath]['ranges']:
                    if dType.split('#')[-1] in ("double","float","decimal","integer","int","long","dateTime","gYear","gMonth","gDay","time","date"):
                        lines.append(path)
                    else:
                        continue
        
        if sphn_connector:
            first_fast = write_min_max_queries(file=file_fast, template=template_fast, concept=concept, lines=lines, concept_str=concept_str, first=first_fast)
            first_ext = write_min_max_queries(file=file_ext, template=template_ext, concept=concept, lines=lines, concept_str=concept_str, first=first_ext)
        else:
            rendered = template.render(Concept=concept, paths=lines)
            file_path = os.path.join(output, 'min_max_predicates', extract_concept_name(concept) + '.rq')
            with open(file_path, 'w') as file:
                file.write(rendered)
    
    if sphn_connector:
        file_fast.write('\n}')
        file_ext.write('\n}')
        file_fast.close()
        file_ext.close()

    return 'Finished'

def render_hasCode(Concepts, DatatypeProperties, ObjectProperties, Restrictions, output, sphn_namespace, sphn_connector = False):
    global global_prefixes
    env = Environment(loader=FileSystemLoader(script_dir))
    os.makedirs(os.path.join(output, 'hasCode'), exist_ok=True)
    if sphn_connector:
        template = env.get_template('hasCode_connector.rq')
        file_path = os.path.join(output, 'hasCode/has-code.rq')
        file = open(file_path, 'w')
        file.write(f"PREFIX sphn:<{sphn_namespace}#>\nPREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\nPREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>\nSELECT *\nWHERE\n{{\n")
    else:
        template = env.get_template('hasCode.rq')
    
    # remove Code concept
    del Concepts[URIRef("https://biomedit.ch/rdf/sphn-schema/sphn#Code")]
    first = True
    for concept in sorted(Concepts.keys()):
        global global_path_list
        global_path_list = []
        if sphn_connector:
            concept_prefix, concept_name = niceify_prefix(prefixable_iri=concept)
            concept_str = f"{concept_prefix}:{concept_name}"
        # print("Running ", concept)
        DFS_Flattening(global_prefixes, Concepts, DatatypeProperties, ObjectProperties, Restrictions, concept, '', [])

        # Here do some exclusions if you need but dont you dare touch my DFS
        # For performance Improvement only run the DFS once and then pass the results to the render funtions
        lines = []
        for path in global_path_list:
            lastPath = path.split('/')[-1][5:]
            if lastPath.startswith('has') and lastPath.endswith('Code'):
                lines.append(path)
            else:
                continue

        if sphn_connector:
            rendered = template.render(Concept=concept, paths=sorted(lines), ConceptStr=concept_str)
            if 'SELECT ?concept ?origin' not in rendered:
                continue
            if first:
                file.write(f"{{{rendered}}}")
                first = False
            else:
                file.write(" UNION \n" + f"{{{rendered}}}")
        else:
            rendered = template.render(Concept=concept, paths=lines)
            file_path = os.path.join(output, 'hasCode', extract_concept_name(concept) + '.rq')
            with open(file_path, 'w') as file:
                file.write(rendered)
    
    if sphn_connector:
        file.write('\n}')
        file.close()

    return 'Finished'


@plac.opt('FileName', "Schema file to load (SPHN or project-spcific)", type=str)
@plac.opt('output', "Schema file to load (SPHN or project-spcific)", type=str)
def main(FileName=[Path('../sphn_rdf_schema.ttl'), Path('../SPHN_dataset_template_2024_1_DMIB_v15_limited_concepts.ttl')], output=script_dir, rootNodes=rootNodes, sphn_connector=False):
    """
    The Sparqler works as follows:
    1. Loading the preprocessed graph and making dictionaries for Concepts, Data- and Object Properties
    2. Loading (and generating) Restrictions 
    3. Run a DFS based on every concept until we reach either a Valueset or a Datatype Property
    4. Generate Sparqls based on those paths

    TODO: adapt the has code, as currently the assumption is that every kind of has*code has the same 3 properties
    
    """
    global global_prefixes
    Singletons = [load_graph(FileName[0:1], 'turtle'), load_graph(FileName[1:], 'turtle')]
    rootNodes, SimplifiedGraph = simplify_schema_graph(sphn_graph=Singletons[0], project_graph=Singletons[1])
    global_prefixes = get_uri(rootNodes)
    sphn_namespace = 'https://biomedit.ch/rdf/sphn-schema/sphn' if 'https://biomedit.ch/rdf/sphn-schema/sphn' in global_prefixes else 'https://biomedit.ch/rdf/sphn-ontology/sphn'
    Concepts = extractConcepts(SimplifiedGraph)
    DatatypeProperties, ObjectProperties = extractProperties(SimplifiedGraph)
    Restrictions = getRestrictions(SimplifiedGraph, Concepts.keys())

    # Moving on to the rendering call
    if not sphn_connector:
        render_flattening(Concepts, DatatypeProperties, ObjectProperties, Restrictions, output)
    render_count(Concepts, DatatypeProperties, ObjectProperties, Restrictions, output, sphn_namespace, sphn_connector)
    render_min_max(Concepts, DatatypeProperties, ObjectProperties, Restrictions, output, sphn_namespace, sphn_connector)
    render_hasCode(Concepts, DatatypeProperties, ObjectProperties, Restrictions, output, sphn_namespace, sphn_connector)
    return


if __name__ == "__main__":
    plac.call(main)