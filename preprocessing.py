# Import modules
import os
import rdflib
from rdflib import RDFS, OWL, RDF, DC, DCTERMS, Graph, URIRef
from urllib import request
import os.path
from pathlib import Path
import time
import logging
import requests
import plac
from query_functions import preprocessingSPHNJena, copySphnIntoGraph, duplicateGraph
import traceback

formatter = logging.Formatter('%(asctime)s %(levelname)s %(filename)s:%(lineno)s: %(message)s')
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
logger.propagate = False
consoleHandler = logging.StreamHandler()
consoleHandler.setFormatter(formatter)
logger.addHandler(consoleHandler)



@plac.opt('schema', "Schema file to load (SPHN or project-spcific)", type=str)
@plac.opt('sphn_schema', "Optional schema file to load (SPHN schema if project-specific schema is provided)", type=str, abbrev='sphn')
@plac.opt('image_links', "Optional image links file to load", type=str)
@plac.opt('link_rdf_schema', "Optional link to the project-specific rdf schema file", type=str)
def main(UID='80085', schema='./genotech_SPHN_dataset_template_2023_23.ttl', sphn_schema='https://sphn.ch/testing#sphn_rdf_schema.ttl', db_host='http://localhost:3030/terminologies/', sparql=False, link_rdf_schema=None, image_links=None, folder='./', logfile=None):
    db_host = db_host.rstrip('/')
    folder = folder.rstrip('/') + '/'
    name = 'preprocessed_sphn_rdf.ttl'
    # Uploading external ontologies and taking care of labels
    # Needs to be updated every time the label graph switches locations !!
    labels = [f'<https://sphn.ch/testing#2023-2_external-terminologies-labels.ttl>']


    # sphn_schema = 'http://example.com/project_preprocessed_rdf_schema'
    if logfile:
        file_handler = logging.FileHandler(folder + logfile)
        file_handler.setLevel(logging.INFO)
        file_handler.setFormatter(formatter)
        logger.addHandler(file_handler)
    logger.info("INFO activated")

    t0 = time.time()

    deleteGraphs([f"<http://example.com/project_{UID}>"], db_host)

    # Upload project graph
    data = open(schema).read()
    encoded_data = data.encode('utf-8')
    headers = {'Content-Type': 'text/turtle; charset=utf-8'}
    logger.info("Call to: ")
    logger.info(f'{db_host}/data?graph=http://example.com/project_{UID}')    
    r = requests.post(f'{db_host}/data?graph=http://example.com/project_{UID}', data=encoded_data, headers=headers)
    print(schema + ':\n' + r.text)
    
    # Generate prefix dictionary
    graph = Graph()
    graph.parse(schema)
    namespace_list = [(i,j) for i,j in graph.namespaces()]
    reverse_prefixes = {}
    for prefix, uri in namespace_list:
        reverse_prefixes[uri] = prefix

    t1 = time.time()
    # Perform pre-processing
    preprocessingSPHNJena(f"<http://example.com/project_{UID}>", labels, db_host, prefixes=reverse_prefixes)

    t2 = time.time()
    url = f"{db_host}/data?graph=http://example.com/project_{UID}"


    logger.info("moving on to Visualization")
    r = requests.get(f'{db_host}/data?graph=http://example.com/project_{UID}')
    file = open(name, "w+", encoding="utf-8")
    file.write(r.text)
    file.close()

    t4 = time.time()
    elapsed_time = t4 - t0
    pylo = t2 - t1
    logger.info(f"Queries time: {pylo:.2f} seconds")
    logger.info(f"Total time: {elapsed_time:.2f} seconds")
    logger.info("Moving on to live service")
    return f"http://example.com/project_{UID}"

def deleteGraphs(list, db_host):
    Query = ""
    for element in list:
        Query += f"DROP GRAPH {element} ; \n"
    r = requests.post(f'{db_host}/update', data={'update': Query}, auth=('admin', 'pw'))


# use plac
if __name__ == "__main__":
    # main()
    plac.call(main)