# SPHN Schema Doc

Documentation and visualization of the [SPHN RDF Schema](https://git.dcc.sib.swiss/sphn-semantic-framework/sphn-schema), based on [pyLODE](https://github.com/RDFLib/pyLODE) and [SPHN RDF Schema images](https://git.dcc.sib.swiss/sphn-semantic-framework/sphn-ontology-images).

The Live OWL Documentation Environment tool ([LODE](https://github.com/essepuntato/LODE)) is used to generate human-readable HTML documents for OWL and RDF ontologies. pyLODE is a complete re-implementation of LODE’s functionality using Python and Python’s RDF manipulation module, [rdflib](https://pypi.org/project/rdflib/). An schema to be documented is parsed and inspected using rdflib and HTML is generated directly, using Python’s [dominate](https://pypi.org/project/dominate/) package.

Some of the pyLODE files were modified for this documentation (e.g. property restrictions were included). The original pyLODE files can be found in the `pylode/original` folder. The modified files are stored under the folder `pylode/modified`.

### Description

The documentation tool takes given ontologies and terminologies as input, manipulates and merges them into a single preprocessed schema and then generates a HTML document.

The html document is structured as follow: It starts with some general information about the schema (URI, version, etc.) and then is divided into five main sections. Each section gives detailed information about the adressed schame components. The end of the html document provides information about namespaces and some legends.

1. Classes: The list of classes defined in the schema contains the sections shown in the Table below:


| Section                            | Description                                                                                                                                             |
| ---------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------- |
| URI                                | URI                                                                                                                                                     |
| Description                        | short description about the class                                                                                                                       |
| Schema representation              | image containing the class schema and its outgoing properties and metadata                                                                              |
| Meaning binding (Equivalent-class) | Link to equivalent class (e.g. SNOMED or LOINC class)                                                                                                   |
| Parents                            | Link to super-classes                                                                                                                                   |
| Children (Sub-classes)             | Link to sub-classes                                                                                                                                     |
| Property (in the domain of)        | List of properties where the class is listed in the domain with given cardinalities, class or datatype information and restriction information (Yes/No) |
| Restrictions                       | details about the restrictions applied on properties in the context of the class (e.g. specified SNOMED codes)                                          |
| Notes                              | Notes for specified properties (allowed coding system or recommended values)                                                                            |
| Used in (In the range of)          | List of properties where the class is listed in the range                                                                                               |

2. Object Properties: provides the list of object properties defined in the schema with their URI, description, super-properties, domain(s) and range.
3. Datatype Properties: provides the list of datatype properties defined in the schema with their URI, description, super-properties, domain(s) and data type.
4. Annotation Properties: provides the list of annotation properties with their URI and description (if provided).
5. Named Individuals: provides the list of named individuals with their URI and the class in which they appear.

For providing a project-specific RDF schema in the PyLODE-based SPHN schema visualization, follow instructions provided in the
[README - User Guide](https://git.dcc.sib.swiss/sphn-semantic-framework/sphn-schema-doc#run-with-project-specific-ontology).
The generated HTML file can then be shared by the project members to anyone who wishes to visualize the project-specific schema in a browser.

# SPHN SPARQL Queries Generator

In addition to the documentation, the tool enables data managers and hospitals to generate queries for flattening SPHN concepts and metadata automatically. Furthermore, it provides statistical queries to acquire insights into data.

'Concept flattening' queries:
Composed of one query per concept/class. The output is a table that 'flattens' the data: each query gives the list of resources defined for that concept together with the direct property values (e.g. for the Biobanksample concept, all resources of type Biobanksample are extracted with the DataProvider object, the Biosample object, the identifier value of the Biobanksample, the AdministrativeCase object and the SubjectPseudoIdentifier object).

Statistical queries:

* Manually built queries, based on the hospIT statistics queries - simply adapted prefixes, class and property names to fit the SPHN RDF schema 2021-1
* Query per concept/class: Counting the instances per concept and predicate
* Query per concept/class: Minimum and maximum of predicates (dates or values)
* Query per concept/class: List and count of all used codes for hasCode

# Preprocessing

A few assumptions are taken in the preprocesing steps that one should be aware of:

* We know the entire list of prefixes for the labels in the label file (Queries insertLabels and insertRecommendedLabels)
* For runs without jena server use input db_host = 'RDFLIB'

# User Guide

The documentation is stored in `sphn_schema_documentation.html`.
Applied ontologies and terminologies are available in the folder `ontology/`.
The `pylode/` folder contains the original pyLODE files (covers profiles, style and templates) and the modified files.

The sparql queries are stored in `sparql-queries/schema-name/query-type`

### Setup

The main dependency is the pyLODE module, which can be installed as follows:

```python
pip3 install -r requirements.txt
```

To use the for the sphn documentation modified content:

* First find out where pylode is installed.

  ```python
  pip3 show pylode  
  ```
* Navigate to the folder of the pylode installation, as determined above.
  For example, the following is a path found on a Mac installation:

  ```
  cd /usr/local/lib/python3.9/site-packages/pylode 
  ```

  > **_NOTE:_**  Making a backup of local files before performing the subsequent changes is recommended. A backup of selected files and folders, based on a macOS installation, can be found as follows:
  >
  > ```
  > /path/to/sphn-schema-doc/pylode/original
  > ```
  >

  To replace the default files cli.py and common.py with those found in the repository, delete the default files, and make a symbolic link in `/path/to/    pylode` as follows:

  ```
  ln -s /path/to/sphn-schema-doc/pylode/modified/cli.py .
  ln -s /path/to/sphn-schema-doc/pylode/modified/common.py .
  ln -s /path/to/sphn-schema-doc/pylode/modified/curies.py .
  ```

  > **_NOTE:_**  For Windows users, the command is
  >
  > ```
  > mklink [/d] <target> <link> 
  > ```
  >
* Next, navigate to the templates folder of the pylode installation, as follows:

  ```
  cd ../templates
  ```

  To replace the default templates for the ontdoc profile with those found in the repository, delete the default  `ontdoc` folder, and make a symbolic link in `/path/to/    pylode/templates` as follows:

  ```
  ln -s /path/to/sphn-schema-doc/pylode/modified/templates/ontdoc .
  ```
* Next, navigate to the style folder of the pylode installation, as follows:

  ```
  cd ../style
  ```

  To replace the default style with the one found in the repository, delete the default  `pylode.css` file, and make a symbolic link in `/path/to/pylode/style` as follows:

  ```
  ln -s /path/to/sphn-schema-doc/pylode/modified/style/pylode.css .
  ```
* Finally, navigate to the profiles folder of the pylode installation, as follows:

  ```
  cd ../profiles
  ```

  To replace the default profile with the one found in the repository, delete the default  `ontdoc.py` and `base.py` file, and make a symbolic link in `/path/to/pylode/profiles` as follows:

  ```
  ln -s /path/to/sphn-schema-doc/pylode/modified/profiles/ontdoc.py .
  ln -s /path/to/sphn-schema-doc/pylode/modified/profiles/base.py .
  ```

### Run

In order to have a performant run it is recommended to setup the schemaforge backend database container. This can be achieved by running a script pulling the container from the gitlab repository:

```
 docker compose -f fuseki-compose.yaml up -d
```

Once this process is complete there are two ways to run the pylode & sparqler. First one can run a project specific pipeline, for this a project specific .ttl file as well as the sphn file need to be passed. The most recent sphn file file can be found under: [Gitlab Link](https://git.dcc.sib.swiss/sphn-semantic-framework/sphn-schema/-/tree/master/rdf_schema). Once all of this is done you can simply run:

```python
python pylode_server.py -s project_specific_schema.ttl -sphn sphn_schema.ttl
```

In case one wants to run the visualization for the sphn schema, you can pass the sphn schema directly, while passing the -p flag as follows:

```python
python pylode_server.py -s sphn_schema.ttl -p 1
```

This reads the respective schemas as well as other terminologies in the `ontology/` folder. The schema passed through the -s flag will then be converted into a schema `prepocessed_schema.ttl` and the sparqls and html documentation are automatically generated in the root folder and the and the `sparql_queries/` folder.

### Local Run

There is a local implementation for the pylode pipeline. In order to run that one should call the `pylode_server.py` with a -d flag to overwrite the default db_host as such:


```
python pylode_server.py -s project_specific_schema.ttl -sphn sphn_schema.ttl -d 'RDFLIB'
python pylode_server.py -s sphn_schema.ttl -p 1 -d 'RDFLIB'

```
Unfortunately the rdflib implementation for querying and graphs is subpar. Therefore, expect much longer processing times when running the pipeline like that.

### File generation for image links

In order to generate the images as well one needs to run:

```
python generate_image_links_file.py "('prefix', rdflib.term.URIRef('associated_uri')" "image_folder/"

```
In our most common scenario this would look like the following:

```
python generate_image_links_file.py "('sphn', rdflib.term.URIRef('https://biomedit.ch/rdf/sphn-schema/sphn#')" "images/"
```
In the folder itself a naming convention exists such that; The images must be named according to their prefix and concept name: `folder/prefix_Concept.png`, or in our case: `images/sphn_Diagnosis.png`

This script outputs an `image_links.ttl` file that can then be passed to the pylode in order to associate images with their respective concepts.

```python
python pylode_server.py -s sphn_schema.ttl -p 1 -i image_links.ttl
```

### Miscellaneous

Running the script with `-h` prints available arguments:

```
usage: pylode_server.py [-h] [-s ./mnew_concepts_proj.ttl] [-sphn sphn_rdf_schema.ttl]
                        [-d http://localhost:3030/terminologies/] [-l None] [-i None] [-p False]
                        [UID] [sphn_preprocessed] [sparql] [folder] [logfile] [external_terminology_file]

positional arguments:
  UID                   [12345]
  sphn_preprocessed     [https://sphn.ch/testing#preprocessed_sphn_rdf_schema.ttl]
  sparql                [True]
  folder                [./]
  logfile               [None]
  external_terminology_file
                        [<https://sphn.ch/testing#2023-2_external-terminologies-labels.ttl>]

options:
  -h, --help            show this help message and exit
  -s ./mnew_concepts_proj.ttl, --schema ./mnew_concepts_proj.ttl
                        Schema file to load (SPHN or project-spcific)
  -sphn sphn_rdf_schema.ttl, --sphn-schema sphn_rdf_schema.ttl
                        Optional schema file to load (SPHN schema if project-specific schema is provided)
  -d http://localhost:3030/terminologies/, --db-host http://localhost:3030/terminologies/
                        Optional db host for the distinction between running it in rdflib or with jena
  -l None, --link-rdf-schema None
                        Optional link to the project-specific rdf schema file
  -i None, --image-links None
                        Optional image links file to load
  -p False, --pure-sphn False
                        Declaration for running the sphn without a project or external terminologies
```

Note that there is a dependency on the `preprocessed_sphn_rdf_schema.ttl` as well as `sphn_rdf_schema.ttl` that has to be available in the container. In order to generate a preprocessed schema, one can either use the `pylode_server.py` or the `preprocessing.py` file.

# Copyright and Licensing information

© Copyright 2025, Personalized Health Informatics Group (PHI), SIB Swiss Institute of Bioinformatics
This software is licensed under the GNU General Public License version 3 (see [Licence](https://git.dcc.sib.swiss/sphn-semantic-framework/sphn-schema-doc/-/blob/master/LICENCE)). It is based on [pyLODE](https://github.com/RDFLib/pyLODE) that is licensed under the BSD 3-Clause licence (see [Licence](https://git.dcc.sib.swiss/sphn-semantic-framework/sphn-schema-doc/-/blob/master/external-licenses/pyLODE-licence/LICENCE)).